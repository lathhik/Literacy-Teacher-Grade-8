<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

//==================== Literacy Announcement Programme  =====================//

Route::get('/', 'HomeController@getWelcome');
Route::group(['namespace' => 'LiteracyAnnouncement', 'prefix' => 'literacy_announcement'], function () {
    Route::get('/', 'HomeController@getLogin');
    Auth::routes([
        'register' => false,
        'reset' => false,
        'verify' => false,
    ]);
    Route::group(['middleware' => 'auth'], function () {
        Route::match(['get', 'post'], 'view-all-members', 'MemberDetailsController@viewAllMembers')->name('view-all-members');
        Route::post('export-member-details', 'MemberDetailsController@exportMemberDetails')->name('export-member-details');
        Route::get('house', 'HouseController@house')->name('house');
        Route::get('show-house/{id}', 'HouseController@showHouse')->name('get-show-house');
        Route::get('view-house-details', 'HouseController@viewHouseDetails')->name('view-house-details');
        Route::get('member-details', 'MemberDetailsController@memberDetail')->name('member-details');

        Route::group(['middleware' => '\App\Http\Middleware\SuperAdminMiddleware'], function () {
            Route::get('/home', 'HomeController@index')->name('home');
            Route::get('add-admin', 'AdminController@addAdmin')->name('add-admin');
            Route::post('add-admin', 'AdminController@addAdminAction')->name('add-admin');
            Route::get('edit-admin/{id}', 'AdminController@editAdmin')->name('edit-admin');
            Route::post('edit-admin-action/{id}', 'AdminController@editAdminAction')->name('edit-admin-action');
            Route::get('delete-admin/{id}', 'AdminController@deleteAdmin')->name('delete-admin');
            Route::get('view-reports', 'HomeController@viewReports')->name('view-reports');
            Route::post('view-reports', 'HomeController@filterReports')->name('post-view-reports');
        });


        Route::group(['middleware' => '\App\Http\Middleware\SuperAdminMiddleware'], function () {
            Route::get('/home', 'HomeController@index')->name('home');
            Route::get('add-admin', 'AdminController@addAdmin')->name('add-admin');
            Route::post('add-admin', 'AdminController@addAdminAction')->name('add-admin');
            Route::get('edit-admin/{id}', 'AdminController@editAdmin')->name('edit-admin');
            Route::post('edit-admin-action/{id}', 'AdminController@editAdminAction')->name('edit-admin-action');
            Route::get('delete-admin/{id}', 'AdminController@deleteAdmin')->name('delete-admin');
            Route::get('view-reports', 'HomeController@viewReports')->name('view-reports');
            Route::post('view-reports', 'HomeController@filterReports')->name('post-view-reports');
        });


        Route::group(['middleware' => '\App\Http\Middleware\CreatorMiddleware'], function () {
            Route::post('add-member-details', 'MemberDetailsController@addMemberDetails')->name('add-member-details');
        });


        Route::group(['middleware' => '\App\Http\Middleware\EditorMiddleware'], function () {
            Route::post('edit-member-details/{id}', 'MemberDetailsController@editMemberDetailAction')->name('edit-member-details');
            Route::get('edit-house-members/{id}', 'MemberDetailsController@editHouseDetails')->name('edit-house-members');
            Route::get('edit-tole/{id}', 'ToleController@editTole')->name('edit-tole');
            Route::post('edit-tole/{id}', 'ToleController@updateTole')->name('post-edit-tole');
            Route::get('edit-ward/{id}', 'WardController@editWard')->name('edit-ward');
            Route::post('edit-ward/{id}', 'WardController@editWardAction')->name('edit-ward');
        });

        Route::group(['middleware' => ['\App\Http\Middleware\EditorMiddleware', '\App\Http\Middleware\CreatorMiddleware']], function () {
            Route::post('add-tole', 'ToleController@addTole')->name('add-tole');
            Route::get('add-tole', 'ToleController@tole')->name('tole');
            Route::get('ward', 'WardController@ward')->name('ward');
            Route::post('add-ward', 'WardController@addWard')->name('add-ward');
        });

        // ajax route
        Route::post('get-tole-ward', 'MemberDetailsController@getToleWard')->name('get-tole-ward');
    });
});

//==================== Teacher Salary Management  =====================//

Route::group(['namespace' => 'TeacherSalaryManagement', 'prefix' => 'teacher_salary_management'], function () {
    Route::get('login', 'TsmAdminAuthController@tsmLogin')->name('tsm-login');
    Route::post('tsm-login', 'TsmAdminAuthController@tsmAdminLogin')->name('tsm-login-action');

    Route::get('tsm-logout', 'TsmAdminAuthController@tsmLogout')->name('tsm-logout');

    Route::group(['middleware' => 'auth:tsmAdmin'], function () {
        Route::get('/', 'HomeController@index')->name('tsm-index');
        Route::get('add-tsm_admin', 'TsmAdminController@addTsmAdmin')->name('add-tsm_admin');
        Route::post('add-tsm-admin', 'TsmAdminController@addTsmAdminAction')->name('add-tsm_admin-action');
        Route::get('edit-tsm_admin/{id}', 'TsmAdminController@editTsmAdmin')->name('edit-tsm_admin');
        Route::post('edit-tsm_admin-action/{id}', 'TsmAdminController@editTsmAdminAction')->name('edit-tsm_admin-action');
        Route::get('delete-tsm_admin/{id}', 'TsmAdminController@deleteTsmAdmin')->name('delete-tsm_admin');

        // ============================== Teacher ==================== //

        Route::get('add-school', 'SchoolController@addSchool')->name('add-school');
        Route::post('add-school-action', 'SchoolController@addSchoolAction')->name('add-school-action');
        Route::get('edit-school/{id}', 'SchoolController@editSchool')->name('edit-school');
        Route::post('edit-school-action/{id}', 'SchoolController@editSchoolAction')->name('edit-school-action');
        Route::get('delete-school/{id}', 'SchoolController@deleteSchool')->name('delete-school');

        Route::get('add-subject', 'SubjectController@addSubject')->name('add-subject');
        Route::post('add-subject-action', 'SubjectController@addSubjectAction')->name('add-subject-action');
        Route::get('edit-subject/{id}', 'SubjectController@editSubject')->name('edit-subject');
        Route::post('edit-subject-action/{id}', 'SubjectController@editSubjectAction')->name('edit-subject-action');
        Route::get('delete-subject/{id}', 'SubjectController@deleteSubject')->name('delete-subject');

        Route::get('add-tsm_class', 'TsmClassController@addTsmClass')->name('add-tsm_class');
        Route::post('add-tsm_class-action', 'TsmClassController@addTsmClassAction')->name('add-tsm_class-action');
        Route::get('edit-tsm_class/{id}', 'TsmClassController@editTsmClass')->name('edit-tsm_class');
        Route::post('edit-tsm_class/{id}', 'TsmClassController@editTsmClassAction')->name('edit-tsm_class-action');
        Route::get('delete-tsm_class/{id}', 'TsmClassController@deleteTsmClass')->name('delete-tsm_class');

        Route::get('add-tsm_grade', 'TsmGradeController@addTsmGrade')->name('add-tsm_grade');
        Route::post('add-tsm_grade-action', 'TsmGradeController@addTsmGradeAction')->name('add-tsm_grade-action');
        Route::get('edit-tsm_grade/{id}', 'TsmGradeController@editTsmGrade')->name('edit-tsm_grade');
        Route::post('edit-tsm_grade-action/{id}', 'TsmGradeController@editTsmGradeAction')->name('edit-tsm_grade-action');
        Route::get('delete-tsm_grade/{id}', 'TsmGradeController@deleteTsmGrade')->name('delete-tsm_grade');

        Route::get('add-teacher', 'TeacherController@addTeacher')->name('add-teacher');
        Route::get('view-teacher', 'TeacherController@viewTeacher')->name('view-teacher');
        Route::post('add-teacher-action', 'TeacherController@addTeacherAction')->name('add-teacher-action');
        Route::get('view-teacher', 'TeacherController@viewTeacher')->name('view-teacher');
        Route::get('edit-teacher/{id}', 'TeacherController@editTeacher')->name('edit-teacher');
        Route::post('edit-teacher-action/{id}', 'TeacherController@editTeacherAction')->name('edit-teacher-action');
        Route::get('delete-teacher/{id}', 'TeacherController@deleteTeacher')->name('delete-teacher');
        Route::get('view-teacher-details/{id}', 'TeacherController@viewTeacherDetails')->name('view-teacher-details');


        Route::get('download-file/{id}', 'TeacherController@downloadFile')->name('download-file');

        // Ajax
        Route::post('delete-teacher-image', 'TeacherController@deleteTeacherImage')->name('delete-teacher-image');


        // ============================== Administrative Staff ==================== //

        Route::get('add-staff-position', 'StaffPositionController@addStaffPosition')->name('add-staff-position');
        Route::post('add-staff-position', 'StaffPositionController@addStaffPositionAction')->name('add-staff-position-action');
        Route::get('edit-staff-position/{id}', 'StaffPositionController@editStaffPosition')->name('edit-staff-position');
        Route::post('edit-staff-position/{id}', 'StaffPositionController@editStaffPositionAction')->name('edit-staff-position-action');
        Route::get('delete-staff-position/{id}', 'StaffPositionController@deleteStaffPosition')->name('delete-staff-position');

        Route::get('add-staff', 'AdminStaffController@addStaff')->name('add-staff');

    });
});
