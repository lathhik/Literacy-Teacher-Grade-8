<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>शिक्षक तलब ब्याबस्थापन</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    @yield('style')
</head>
<body>
<div id="app">
    <div class="d-flex" id="wrapper">
        @if(Auth::guard('tsmAdmin')->user())
            @include('teacher_salary_management.partials.side-nav')
        @endif
        <div id="page-content-wrapper">
            @if(Auth::guard('tsmAdmin')->user())
                @include('teacher_salary_management.partials.top-nav')
            @endif
            @yield('content')
        </div>
    </div>
</div>


<!-- /#wrapper -->
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/repeater.js') }}"></script>

<!-- Menu Toggle Script -->
<script>
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
</script>
@yield('script')
</body>
</html>
