@extends('layouts.literacy_announcement')
@section('content')
    <div class="container">
        <div class="mt-4">
            @include('messages.succFail')
        </div>
        <div class="row mt-4">
            @if(Auth::User()->privilege != 'editor')
                <div class="col-md-5">
                    <div class="card">
                        <div class="card-header text-center">टोल छान्नुहोस</div>
                        <div class="card-body">
                            <form method="post" action="{{route('add-tole')}}">
                                @csrf
                                <div class="form-group">
                                    <label for="formGroupExampleInput">टोल:</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput"
                                           placeholder="टोल हाल्नुहोस" name="tole">
                                    @if($errors->has('tole'))
                                        <p class="text-danger">{{$errors->first('tole')}}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="formGroupExampleInput">वडा</label>
                                    <select class="form-control" name="ward_id">
                                        <option value="" disabled selected>वडा नं छान्नुहोस.</option>
                                        @foreach($wards as $ward)
                                            <option value="{{$ward->id}}">{{$ward->ward_no}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('ward_id'))
                                        <p class="text-danger">{{$errors->first('ward_id')}}</p>
                                    @endif
                                </div>
                                <div>
                                    <button class="btn btn-success float-right" type="submit">बुझाउनुहोस</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            @endif
            <div class="{{(Auth::User()->privilege != 'editor')?'col-md-7':'col-md-12'}}">

                <div class="card">
                    <div class="card-header text-center">टोल</div>
                    <div class="card-body">
                        <table class="table table-bordered text-center  table-sm">
                            <thead>
                            <th>क्रं.स</th>
                            <th>टोल</th>
                            <th>वडा नं.</th>
                            @if(Auth::User()->privilege != 'creator')
                                <th>कार्य</th>
                            @endif
                            </thead>
                            <tbody>
                            @foreach($toles as $tole)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$tole->tole}}</td>
                                    <td>{{$tole->ward->ward_no}}</td>
                                    @if(Auth::User()->privilege != 'creator')
                                        <td>
                                            <a href="{{route('edit-tole',$tole->id)}}" class="btn btn-sm btn-success">
                                                <li class="fa fa-edit"></li>
                                            </a>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>

    </div>
@stop
