@extends('layouts.literacy_announcement')
@section('content')
    <div class="container">
        <div class="mt-4">
            @include('messages.succFail')
        </div>
        <div class="row mt-4">
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header text-center">वडा सम्पादन</div>
                    <div class="card-body">
                        <form method="post" action="{{route('edit-ward',$ward->id)}}">
                            @csrf
                            <div class="form-group">
                                <label for="formGroupExampleInput">वडा नं:</label>
                                <input type="text" class="form-control" id="formGroupExampleInput"
                                       placeholder="Input ward no" name="ward" value="{{$ward->ward_no}}">
                                @if($errors->has('ward'))
                                    <p class="text-danger">{{$errors->first('ward')}}</p>
                                @endif
                            </div>
                            <div>
                                <button class="btn btn-success float-right" type="submit">बुझाउनुहोस</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>
@stop
