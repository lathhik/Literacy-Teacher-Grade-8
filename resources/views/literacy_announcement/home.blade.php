@extends('layouts.literacy_announcement')
@section('style')
{{--    <link rel="stylesheet" type="text/css" href="{{asset('public/css/print.css')}}" media="print"/>--}}
    @stop

@section('content')

    <div class="container">
        <div class="row mt-4">
            <div class="col-md-3 d-print-none mb-3">
                <div class="card">
                    <div class="card-header">कुल वडा संख्या
                    </div>
                    <div class="card-body">
                        <h3>{{$ward_count}}</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-3 d-print-none mb-3">
                <div class="card">
                    <div class="card-header">कुल टोल संख्या
                    </div>
                    <div class="card-body">
                        <h3>{{$tole_count}}</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-3 d-print-none mb-3">
                <div class="card">
                    <div class="card-header">कुल घर संख्या
                    </div>
                    <div class="card-body">
                        <h3>{{$house_count}}</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-3 d-print-none mb-3">
                <div class="card">
                    <div class="card-header">कुल सदस्य संख्या
                    </div>
                    <div class="card-body">
                        <h3>{{$member_count}}</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-3 d-print-none mb-3">
                <div class="card">
                    <div class="card-header">कुल पुरुष प्रतिशत
                    </div>
                    <div class="card-body">
{{--                        <h3>{{number_format(($male_count /$member_count)*100,'2','.','')}} %</h3>--}}
                    </div>
                </div>
            </div>
            <div class="col-md-3 d-print-none mb-3">
                <div class="card">
                    <div class="card-header">कुल महिला प्रतिशत
                    </div>
                    <div class="card-body">
{{--                        <h3>{{number_format(($female_count /$member_count)*100,'2','.','')}} %</h3>--}}
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
