@extends('layouts.literacy_announcement')
@section('content')
    <div class="container">
        <div class="mt-4">
            @include('messages.succFail')
        </div>
        <div class="row mt-4">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-center">घरको विवरण थप्नुहोस</div>
                    <div class="card-body">
                        <form method="post" action="{{route('add-member-details')}}">
                            @csrf
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="formGroupExampleInput">वडा नं.: <span class="text-danger">*</span></label>
                                    <select name="ward_no" id="ward" class="form-control" required>
                                        <option value="" disabled selected>वडा नं. छान्नुहोस</option>
                                        @foreach($wards as $ward)
                                            <option value="{{$ward->id}}">{{$ward->ward_no}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('ward_no'))
                                        <p class="text-danger">{{$errors->first('ward_no')}}</p>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="formGroupExampleInput">टोल: <span class="text-danger">*</span></label>
                                    <select name="tole" id="tole" class="form-control" required>
                                        <option value="" disabled selected>टोल छान्नुहोस</option>
                                        @foreach($toles as $tole)
                                            <option value="{{$tole->id}}">{{$tole->tole}}</option>
                                        @endforeach                                    </select>
                                    @if($errors->has('tole'))
                                        <p class="text-danger">{{$errors->first('tole')}}</p>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="formGroupExampleInput">घर नं.:</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput"
                                           placeholder="घर नं हाल्नुहोस." name="house_no">
                                    @if($errors->has('house_no'))
                                        <p class="text-danger">{{$errors->first('house_no')}}</p>
                                    @endif
                                </div>
                            </div>
                            <hr>
                            <h1 style="text-align: center">परिवारको विवरण</h1>
                            <h4>घरमुलिको विवरण</h4>
                            <div id="memberDetails">
                                <div id="headDetails"></div>
                                <hr>
                                <h4>अन्य सदस्यको विवरण</h4>
                            </div>
                            <div class="mt-2">
                                <button type="button" class="btn btn-md btn-success" id="addRepeater">थप</button>
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-success float-right"  type="submit">बुझाउनुहोस</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="repeater" style="display: none">
        <div class="items">
            <hr>
            <div class="item-content">
                <div class="row">
                    <div class="form-group col-md-4">
                        <label for="">पहिलो नाम: <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="" name="first_name[]"
                               placeholder="पहिलो नाम हाल्नुहोस" required>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="">बिचको नाम:</label>
                        <input type="text" class="form-control" id="" name="middle_name[]"
                               placeholder="बिचको नाम हाल्नुहोस" >
                    </div>
                    <div class="form-group col-md-4">
                        <label for="">थर: <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="" name="last_name[]"
                               placeholder="थर हाल्नुहोस" required>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="">उमेर: <span class="text-danger">*</span></label>
                        <input type="number" class="form-control memberAge"  name="age[]" min="1"
                               placeholder="उमेर हाल्नुहोस" required>
                    </div>
                    <div class="form-group col-md-4 head-detail">
                        <label for="formGroupExampleInput">घरमुलिसंगको नाता</label><br>
                        <select name="rel_to_head[]" id="" class="form-control rel_to_head">
                            <option value="">नाता छान्नुहोस</option>
                            <option value="husband">पति</option>
                            <option value="wife">पत्नि</option>
                            <option value="father">बुवा</option>
                            <option value="mother">आमा</option>
                            <option value="son">छोरा</option>
                            <option value="daughter">छोरी</option>
                            <option value="big-brother">दाई</option>
                            <option value="small-brother">भाई</option>
                            <option value="big-sister">दिदि</option>
                            <option value="small-sister">बहिनी</option>
                            <option value="vauju">भाउजु</option>
                            <option value="buhari">बुहारी</option>
                            <option value="grandson">नाति</option>
                            <option value="granddaughter">नातिनी</option>
                            <option value="granddaughter-in-law">नातिनी बुहारी</option>
                            <option value="vatij">भतिज</option>
                            <option value="vatiji">भतिजी</option>
                            <option value="kaka">काका</option>
                            <option value="kaki">काकी</option>
                            <option value="son-in-law">ज्‌वाइ</option>
                            <option value="brother-in-law">भिनाजु</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="">लिङ्ग: <span class="text-danger">*</span></label><br>
                        <input type="checkbox" value="male" name="gender[]"
                               class="mt-2 chb checkboxRequired" required>
                        <label for="male">
                            पुरुष </label>&nbsp;&nbsp;
                        <input type="checkbox" value="female" name="gender[]" class="chb checkboxRequired" required>
                        <label for="female">
                            महिला </label>&nbsp;
                        <input type="checkbox" value="other" name="gender[]" class="chb checkboxRequired" required>
                        <label for="other">
                            अन्य </label>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="">साक्षरता: <span class="text-danger">*</span></label><br>
                        <input type="checkbox" value="1" class="mt-2 lit_yes chb checkboxRequired" name="literacy[]" required>
                        <label for="">
                            साक्षर </label>&nbsp;
                        <input type="checkbox" value="0" class="lit_no chb checkboxRequired" name="literacy[]"  required>
                        <label for="">
                            निरक्षर </label>&nbsp;
                    </div>
                    <div class="form-group col-md-4 grade" id="" style="display: none">
                        <label for="formGroupExampleInput">तह: <span class="text-danger">*</span></label><br>
                        <select name="grade[]" id="" class="form-control">
                            <option value="select" selected >तह छान्नुहोस</option>
                            <option value="basic">आधारभुत</option>
                            <option value="secondary">माध्यमिक</option>
                            <option value="higher_education">उच्च शिक्षा</option>
                            <option value="informal">अनौपचारिक</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4 school"  style="display: none">
                        <label for="">हाल  बिद्यालय गएको :</label><br>
                        <input type="checkbox" value="1" class="mt-2 chb" name="school_status[]">
                        <label for="">
                            छ </label>&nbsp;
                        <input type="checkbox" value="0" class="chb" name="school_status[]">
                        <label for="">
                            छैन </label>&nbsp;
                        <input type="checkbox" value="" class="chb" name="school_status[]" checked style="display: none">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-8">
                        <label for="formGroupExampleInput">कैफियत:</label><br>
                        <textarea name="remarks[]" id="" cols="50" rows="5"></textarea>
                    </div>
                </div>
            </div>
            <button type="button" class="btn btn-danger remove-btn">हटाउनुहोस</button>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            repeatMemberDetailsWitoutRemove();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $('#ward').on('change', function () {
                var wardId = $(this).val();
                var ajxRoute = '{{route('get-tole-ward')}}';
                $.ajax({
                    url: ajxRoute,
                    type: 'POST',
                    data: {_token: CSRF_TOKEN, id: wardId},
                    dataType: 'JSON',
                    success: function (data) {
                        $('#tole').empty();
                        $('#tole').append('<option value="" disabled selected>टोल छान्नुहोस</option>');
                        $.each(data, function (key, value) {
                            $('#tole').append('<option value="' + value.id + '">' + value.tole + '</option>')
                        })
                    }
                })
            });

            $('#addRepeater ').on('click', function () {
                repeatMemberDetails();
            });
            $('#memberDetails').on('click', '.remove-btn', function () {
                $(this).parent().remove();
            });
            $('#memberDetails').on('click', '.lit_yes', function () {
                let parent = $(this).closest('.row');
                parent.find('.grade').show();
                if(parent.find('.memberAge').val()<=18){
                    parent.find('.school').show();
                }
            });
            $('#memberDetails').on('click', '.lit_no', function () {
                $(this).parent().parent().parent().find('.grade').hide();
                $(this).parent().parent().parent().find('.school').hide();
            });
            $('#memberDetails').on('change', '.chb', function () {
                $(this).parent().find(".chb").prop('checked', false);
                $(this).prop('checked', true);
            });
            $('#memberDetails').on('change','.memberAge',function () {
               if($(this).val()<= 18 && $(this).closest('.row').find('.lit_yes').prop('checked')){
                   $(this).closest('.row').find('.school').show();
               }else{
                   $(this).closest('.row').find('.school').hide();
               }
            });
            $('#memberDetails').on('change','.checkboxRequired',function () {
               $(this).siblings('input').removeAttr('required');
            });
            $('#memberDetails').on('change', 'select.rel_to_head', function () {
                var relation = $(this).val();
                let female = ['wife', 'mother', 'daughter', 'big-sister', 'small-sister', 'vauju', 'buhari', 'granddaughter', 'granddaughter-in-law', 'vatiji', 'kaki'];
                if(relation){
                    if(female.includes(relation)){
                        let femaleInput = $(this).closest('.row').find('input[value="female"]');
                        femaleInput.parent().find(".chb").prop('checked', false);
                        femaleInput.prop('checked',true);
                        femaleInput.siblings('input').removeAttr('required');

                    }else{
                        let maleInput = $(this).closest('.row').find('input[value="male"]');
                        maleInput.parent().find(".chb").prop('checked', false);
                        maleInput.prop('checked',true);
                        maleInput.siblings('input').removeAttr('required');

                    }
                }else{
                    let blankInput = $(this).closest('.row').find('input[value="female"]');
                    blankInput.parent().find(".chb").prop('checked', false);
                }

            });

            function repeatMemberDetails() {
                var repeater = $('#repeater .items').clone();
                $('#memberDetails').append(repeater);
            }
            function repeatMemberDetailsWitoutRemove() {
                var repeater = $('#repeater .items').clone();
                repeater.find(".remove-btn").remove();
                repeater.find(".head-detail")   .remove();
                repeater.append('<input name="rel_to_head[]" style="display:none;" value="self">');
                $('#headDetails').append(repeater);
            }
        });
    </script>
@stop
