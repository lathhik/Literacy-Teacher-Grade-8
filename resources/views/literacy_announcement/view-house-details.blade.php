@extends('layouts.literacy_announcement')
@section('content')
    <div class="container mt-4">
        <h3 style="text-align: center"><b>घरको विवरण</b></h3>
        <table class="table-bordered text-center table-sm" style="width: 100%   ">
            <thead>
            <th>सि.नं.</th>
            <th>घर नं.</th>
            <th>घरमुलिको विवरण</th>
            <th>टोल</th>
            <th>परिवार संख्या</th>
            <th>कार्य</th>
            </thead>
            <tbody>
            @foreach($houses as $house)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$house->house_no}}</td>
                    <td>{{$house->members->where('relation_to_head','self')->first()->full_name}}</td>
                    <td>{{$house->tole->tole}}</td>
                    <td>{{count($house->members)}}</td>
                    <td>
                        <a href="{{route('get-show-house',$house->id)}}" class="btn btn-sm btn-info">
                            <span class="fa fa-eye"></span>
                        </a>
                        @if(Auth::User()->privilege != 'creator')
                            <a href="{{route('edit-house-members',$house->id)}}" class="btn btn-sm btn-success">
                                <span class="fa fa-edit"></span>
                            </a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop
