@extends('layouts.literacy_announcement')
@section('content')
    @php
        $gender = [
        'male'=>'पुरुष',
        'female'=>'महिला',
        'other'=>'अन्य'
        ];
    @endphp
    <div class="container mt-4">
        <h3 style="text-align: center"><b>सम्पुर्ण सदस्यहरुको विवरण</b></h3>
        <div class="row">
            <div class="col-md-12">
                <form action="" method="GET" id="filterForm">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="ward">वडा</label>
                                <select name="ward" id="ward" class="form-control">
                                    <option value="all" selected>सबै</option>
                                    @foreach($wards as $ward)
                                        <option
                                            value="{{$ward->id}}" {{$ward->id==$selectedVars['ward'] ? 'selected' :''}}>{{$ward->ward_no}}</option>
                                        @php
                                            if($ward->id==$selectedVars['ward']){
                                            $selectedWard = $ward;
                                            }
                                        @endphp
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 " id="toleInput">
                            <div class="form-group">
                                <label for="tole">टोल</label>
                                <select name="tole" id="tole" class="form-control">
                                    @if($selectedVars['ward'] != 'all')
                                        <option value="all" {{$selectedVars['tole']=='all'?'selected':''}}>सबै</option>
                                        @foreach($selectedWard->toles as $tole)
                                            <option
                                                value="{{$tole->id}}" {{$selectedVars['tole']==$tole->id ? 'selected' :''}}>{{$tole->tole}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="literacy">साक्षरता</label>
                                <select name="literacy" id="literacy" class="form-control">
                                    <option value="both" {{$selectedVars['literacy']=='both'?'selected':''}}>दुवै
                                    </option>
                                    <option value="literate" {{$selectedVars['literacy']=='literate'?'selected':''}}>
                                        साक्षर
                                    </option>
                                    <option
                                        value="illiterate" {{$selectedVars['literacy']=='illiterate'?'selected':''}}>
                                        निरक्षर
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 row">
                            <div class="col-md-6">
                                <label for="">उमेर देखि</label>
                                <input type="number" class="form-control"
                                       name="ageFrom" id="ageFrom"
                                       value="{{isset($selectedVars['ageFrom'])?$selectedVars['ageFrom'] : null }}">
                            </div>
                            <div class="col-md-6">
                                <label for="">उमेर सम्म</label>
                                <input type="number" class="form-control"
                                       name="ageTo" id="ageTo"
                                       value="{{isset($selectedVars['ageTo'])?$selectedVars['ageTo'] : null }}">
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="ml-4 form-group">
                            <button class="btn btn-success mr-2" id="filterButton">फिल्टर</button>
                            <button class="btn btn-info" id="importExcel">Excel मा Export गर्नुहोस</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-12">
                <div class="">
                    <table class="table-bordered text-center table-sm" style="width: 100%   ">
                        <thead>
                        <th>सि.नं.</th>
                        <th>पुरा नामथर</th>
                        <th>उमेर</th>
                        <th>लिङ्ग</th>
                        <th>साक्षरता</th>
                        <th>टोल</th>
                        <th>वडा नं.</th>
                        </thead>
                        <tbody>
                        @foreach($members as $member)
                            <tr style={{($member->literacy == 0)?'background-color:#ebd1d2 ':''}}>
                                <td>{{$loop->iteration}}</td>
                                <td>
                                    {{$member->full_name}}
                                </td>
                                <td>{{$member->age}}</td>
                                <td>
                                    {{$gender[$member->gender]}}
                                </td>
                                <td>
                                    @if($member->literacy == 1)
                                        {{'साक्षर'}}
                                    @else
                                        {{'निराक्षर'}}
                                    @endif
                                </td>
                                <td>
                                    {{$member->house->tole->tole}}
                                </td>
                                <td>
                                    {{$member->house->tole->ward->ward_no}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>

        </div>
    </div>
    <div class="float-right mt-2 mr-4">
        {{$members->appends($_GET)->links()}}
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            if ($('#ward').val() == 'all') {
                $('#toleInput').hide();
            }

            function validAgeGroup() {
                if(parseInt($('#ageFrom').val())>parseInt($('#ageTo').val())){
                    return false;
                }
                return true;
            }

            $('#importExcel').on('click', function (e) {
                if(validAgeGroup()){
                    $('#filterForm').attr('action', "{{route('export-member-details')}}").submit();
                }else{
                    e.preventDefault();
                    alert('उमेर समुह क्रमस सानो देखि ठुलो मिलाएर लेख्नुहोस');
                }

            });
            $('#filterButton').on('click', function (e) {
                if(validAgeGroup()){
                    $('#filterForm').attr('action', "").submit();
                }else{
                    e.preventDefault();
                    alert('उमेर समुह क्रमस सानो देखि ठुलो मिलाएर लेख्नुहोस');
                }
            });
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $('#ward').on('change', function () {
                var wardId = $(this).val();
                if (wardId == 'all') {
                    $('#toleInput').hide();
                    $('#tole').empty();
                } else {
                    $('#toleInput').show();
                    retrieveTole(wardId);
                }
            });

            function retrieveTole(wardId) {
                var ajxRoute = '{{route('get-tole-ward')}}';
                $.ajax({
                    url: ajxRoute,
                    type: 'POST',
                    data: {_token: CSRF_TOKEN, id: wardId},
                    dataType: 'JSON',
                    success: function (data) {
                        $('#tole').empty();
                        $('#tole').append('<option value="all" selected>सबै</option>');
                        $.each(data, function (key, value) {
                            $('#tole').append('<option value="' + value.id + '">' + value.tole + '</option>')
                        })
                    }
                })
            }
        });
    </script>
@stop
