<!-- Sidebar -->
<div class="border-right d-print-none" id="sidebar-wrapper">
    <div class="sidebar-heading">साक्षर घोसणा अभियान</div>
    <div class="list-group list-group-flush">

        @if(Auth::user()->privilege == 'super_admin')
            <a href="{{route('home')}}"
               class="list-group-item list-group-item-action {{ request()->is('chandragiri/home') ? 'active' : '' }}">ड्यासबोर्ड</a>
        @endif

        <a href="{{route('ward')}}"
           class="list-group-item list-group-item-action {{ request()->is('chandragiri/ward') ? 'active' : '' }}"> वडा थप/सुची</a>
        <a href="{{route('tole')}}"
           class="list-group-item list-group-item-action {{ Request::is('chandragiri/add-tole') ? 'active' : '' }}">टोल
            थप/सुची</a>

        @if(Auth::user()->privilege != 'editor')

            <a href="{{route('member-details')}}"
               class="list-group-item list-group-item-action {{ Request::is('chandragiri/member-details') ? 'active' : '' }}">घर
                विवरण
                थप</a>
        @endif

        <a href="{{route('view-house-details')}}"
           class="list-group-item list-group-item-action {{ Request::is('chandragiri/view-house-details') ? 'active' : '' }}">घर
            विवरण थप/
            सम्पादन</a>
        @if(Auth::user()->privilege == 'super_admin')
            <a href="{{route('view-all-members')}}"
               class="list-group-item list-group-item-action  {{ Request::is('chandragiri/view-all-members') ? 'active' : '' }}">परिवार
                सदश्य विवरण</a>
            <a href="{{route('view-reports')}}"
               class="list-group-item list-group-item-action  {{ Request::is('chandragiri/view-reports') ? 'active' : '' }}">रिपोर्टहरू</a>
            <a href="{{route('add-admin')}}" class="list-group-item list-group-item-action bg-light">प्रयोगकर्ता</a>
        @endif
    </div>
</div>
<!-- /#sidebar-wrapper -->
