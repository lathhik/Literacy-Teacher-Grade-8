@extends('layouts.literacy_announcement')
@section('content')
    <div class="container">
        <div class="mt-4">
            @include('messages.succFail')
        </div>
        <div class="row mt-4">
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header text-center">प्रयोगकर्ता थप्पनुहोस</div>
                    <div class="card-body">
                        <form method="post" action="{{route('add-admin')}}">
                            @csrf
                            <div class="form-group">
                                <label for="formGroupExampleInput">पुरा नामथर:</label>
                                <input type="text" class="form-control" id="formGroupExampleInput"
                                       placeholder="पुरा नाम " name="full_name" value="{{old('full_name')}}">
                                @if($errors->has('full_name'))
                                    <p class="text-danger">{{$errors->first('full_name')}}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">ईमेल:</label>
                                <input type="email" class="form-control" id="formGroupExampleInput"
                                       placeholder="ईमेल" name="email" value="{{old('email')}}">
                                @if($errors->has('email'))
                                    <p class="text-danger">{{$errors->first('email')}}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">बिशेषाधिकार:</label>
                                <select name="privilege" id="" class="form-control">
                                    <option value="" selected disabled> बिशेषाधिकार छनाोट गर्नुहोस</option>
                                    <option value="super_admin">प्रमानित गर्ने</option>
                                    <option value="creator">दर्ता गर्ने</option>
                                    <option value="editor">रुजु गर्ने</option>
                                </select>
                                @if($errors->has('email'))
                                    <p class="text-danger">{{$errors->first('email')}}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">पासवर्ड:</label>
                                <input type="password" class="form-control" id="formGroupExampleInput"
                                       placeholder="**********" name="password">
                                @if($errors->has('password'))
                                    <p class="text-danger">{{$errors->first('password')}}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">पासवर्ड प्रमाणिकरण:</label>
                                <input type="password" class="form-control" id="formGroupExampleInput"
                                       placeholder="*********" name="password_confirmation">
                            </div>
                            <div>
                                <button class="btn btn-success float-right" type="submit">बुझाउनुहोस</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
            <div class="col-md-7">

                <div class="card">
                    <div class="card-header text-center">प्रयोगकर्ता विबरण</div>
                    <div class="card-body">
                        <table class="table table-bordered text-center  table-sm">
                            <thead>
                            <th>क्र.सं</th>
                            <th>पुरा नामथर</th>
                            <th>ईमेल</th>
                            <th>बिशेषाधिकार</th>
                            <th>कार्य</th>
                            </thead>
                            <tbody>
                            @foreach($admins as $admin)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$admin->name}}</td>
                                    <td>{{$admin->email}}</td>
                                    <td>{{$admin->privilege}}</td>
                                    <td>
                                        <a class="btn btn-sm btn-success" href="{{route('edit-admin',$admin->id)}}"><i
                                                class="fa fa-edit"></i></a>
                                        <a class="btn btn-sm btn-danger" href="{{route('delete-admin',$admin->id)}}"><i
                                                class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>

            </div>
        </div>

    </div>
@stop

