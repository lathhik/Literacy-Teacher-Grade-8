@extends('layouts.literacy_announcement')
@section('content')
    <div class="container">
        <div class="mt-4">
            @include('messages.succFail')
        </div>
        <div class="row mt-4 ">
            <div class="col-md-8 ml-2">
                <div class="card">
                    <div class="card-header text-center">Edit Admin</div>
                    <div class="card-body">
                        <form method="post" action="{{route('edit-admin-action',$admin->id)}}">
                            @csrf
                            <div class="form-group">
                                <label for="formGroupExampleInput">Full Name:</label>
                                <input type="text" class="form-control" id="formGroupExampleInput"
                                       placeholder="Input Full Name" name="full_name" value="{{$admin->name}}">
                                @if($errors->has('full_name'))
                                    <p class="text-danger">{{$errors->first('full_name')}}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">Email</label>
                                <input type="email" class="form-control" id="formGroupExampleInput"
                                       placeholder="Input Email" name="email" value="{{$admin->email}}">
                                @if($errors->has('email'))
                                    <p class="text-danger">{{$errors->first('email')}}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">Privilege</label>
                                <select name="privilege" id="" class="form-control">
                                    <option value="" selected disabled> बिशेषाधिकार छनाोट गर्नुहोस</option>
                                    <option value="super_admin" {{($admin->privilege == 'super_admin')?'selected':''}}>प्रमानित गर्ने</option>
                                    <option value="creator" {{($admin->privilege == 'creator')?'selected':''}}>दर्ता गर्ने</option>
                                    <option value="editor" {{($admin->privilege == 'editor')?'selected':''}}>रुजु गर्ने</option>
                                </select>
                                @if($errors->has('privilege'))
                                    <p class="text-danger">{{$errors->first('privilege')}}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">Password:</label>
                                <input type="password" class="form-control" id="formGroupExampleInput"
                                       placeholder="**********" name="password">
                                @if($errors->has('password'))
                                    <p class="text-danger">{{$errors->first('password')}}</p>
                                @endif
                            </div>
                            <div>
                                <button class="btn btn-success float-right" type="submit">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

