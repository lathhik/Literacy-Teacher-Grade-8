@extends('layouts.literacy_announcement')
@section('content')
    <div class="container">
        <div class="mt-4">
            @include('messages.succFail')
        </div>
        <div class="row mt-4">
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header text-center">घरको विवरण</div>
                    <div class="card-body">
                        <form method="post" action="{{route('add-tole')}}">
                            @csrf
                            <div class="form-group">
                                <label for="formGroupExampleInput">घर नं:</label>
                                <input type="text" class="form-control" id="formGroupExampleInput"
                                       placeholder="Input Tole" name="tole">
                                @if($errors->has('tole'))
                                    <p class="text-danger">{{$errors->first('tole')}}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">वडा</label>
                                <select class="form-control" name="ward_id">
                                    <option value="" disabled selected>वडा नं. छान्नुहोस</option>
                                    @foreach($wards as $ward)
                                        <option value="{{$ward->id}}">{{$ward->ward_no}}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('ward_id'))
                                    <p class="text-danger">{{$errors->first('ward_id')}}</p>
                                @endif
                            </div>
                            <div>
                                <button class="btn btn-success float-right" type="submit">बुझाउनुहोस</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
            <div class="col-md-7">

                <div class="card">
                    <div class="card-header text-center">टोल</div>
                    <div class="card-body">
                        <table class="table table-bordered text-center  table-sm">
                            <thead>
                            <th>सि.नं.</th>
                            <th>टोल</th>
                            <th>वडा नं.</th>
                            </thead>
                            <tbody>
                            @foreach($toles as $tole)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$tole->tole}}</td>
                                    <td>{{$tole->ward->ward_no}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>

            </div>
        </div>

    </div>
@stop
