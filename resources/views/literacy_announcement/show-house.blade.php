@extends('layouts.literacy_announcement')
@section('style')
    <style>
        @page {
            size: A4;
            margin: 0;
        }

        @media print {
            html, body {
                width: 210mm;
                height: 297mm;
            }

            .print-body {
                margin-top: 100px;
                margin-left: 10%;
                margin-right: 10%;
            }

            .card {
                border: hidden;
            }
        }
    </style>
@stop

@section('content')
    @php
        $relation_to_head =[
        "self"=>"स्वयम",
        "husband"=>'पति',
        "wife"=>"पत्नि",
        "father"=>"बुवा",
        "mother"=>"आमा",
        "son"=>"छोरा",
        "daughter"=>"छोरी",
        "big-brother"=>"दाई",
        "small-brother"=>"भाई",
        "vauju"=>"भाउजु",
        "buhari"=>"बुहारी",
        "grandson"=>"नाति",
        "granddaughter"=>"नाति",
        "vatij"=>"भतिज",
        "vatiji"=>"भतिजी",
        "kaka"=>"काका",
        "kaki">"काकी"

            ];

    $grade = [
    'basic'=>'आधारभुत',
    'higher_education'=>'उच्च शिक्षा',
    'secondary'=>'माध्यमिक',
    'informal'=>'अनोैपचारिक',

    ];

    $gender = [
        'male'=>'पुरुष',
        'female'=>'महिला',
        'others'=>'अन्य'
    ]

    @endphp
    <div class="container">
        <div class="row">
            <div class="offset-md-2 col-md-8 card print-body">
                <div class="card-body row">
                    <div class="col-md-12 text-center">
                        <h4>चन्द्रागिरी नगरपालिका </h4>
                        <h4>साक्षर घोषणा अभियान - २०७६ </h4>
                        <h4>निरक्षरताको नामनामेसी संकलन घरधुरी सर्वेक्षण फारम - २०७६</h4>
                    </div>
                    <div class="col-md-12 row mt-4">
                        <div class="col-md-6">वडा नं: {{$house->tole->ward->ward_no}}</div>
                        <div class="col-md-6 text-right">गाउँ टोल:{{$house->tole->tole}}</div>
                    </div>
                    <div class="col-md-12 row my-4 ">
                        <div class="col-md-6">घरमुलीको नामथर
                            : {{$house->members->where('relation_to_head','self')->first()->full_name}}</div>
                    </div>

                    <table class="table table-bordered text-center">
                        <thead>
                        <tr>
                            <td>#</td>
                            <td>पुरा नाम</td>
                            <td>लिङ्ग</td>
                            <td>उमेर</td>
                            <td>घरमुलिसंगको नाता</td>
                            <td>निरक्षर</td>
                            <td>साक्षर</td>
                            <td>तह</td>
                            <td>कैफियत</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($house->members as $member)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$member->full_name}}</td>
                                <td>
                                    @foreach($gender as $key => $value)
                                        @if($member->gender == $key)
                                            {{$value}}
                                        @endif
                                    @endforeach
                                </td>
                                <td>{{$member->age}}</td>
                                <td>
                                    @foreach($relation_to_head as $key => $value)
                                        @if($member->relation_to_head == $key)
                                            {{$value}}
                                        @endif
                                    @endforeach
                                </td>
                                <td>
                                    @if($member->literacy==0)
                                        <span style="font-weight: 900">&#10003;</span>
                                    @endif
                                </td>
                                <td>
                                    @if($member->literacy==1)
                                        <span style="font-weight: 900">&#10003;</span>
                                    @endif
                                </td>
                                <td>
                                    @foreach($grade as $key => $value)
                                        @if($member->grade == $key)
                                            {{$value}}
                                        @endif
                                    @endforeach
                                </td>
                                <td>{{$member->remarks}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <button class="mx-auto btn btn-primary btn-sm d-print-none" onclick="window.print()">Print
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
@stop
