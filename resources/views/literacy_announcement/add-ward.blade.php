@extends('layouts.literacy_announcement')
@section('content')
    <div class="container">
        <div class="mt-4">
            @include('messages.succFail')
        </div>
        <div class="row mt-4">
            @if(Auth::User()->privilege != 'editor')
                <div class="col-md-5">
                    <div class="card">
                        <div class="card-header text-center">वडा थप्नुहोस</div>
                        <div class="card-body">
                            <form method="post" action="{{route('add-ward')}}">
                                @csrf
                                <div class="form-group">
                                    <label for="formGroupExampleInput">वडा नं.:</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput"
                                           placeholder="वडा नं. हाल्नुहोस" name="ward">
                                    @if($errors->has('ward'))
                                        <p class="text-danger">{{$errors->first('ward')}}</p>
                                    @endif
                                </div>
                                <div>
                                    <button class="btn btn-success float-right" type="submit">बुझाउनुहोस</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            @endif

            <div class="{{(Auth::User()->privilege != 'editor')?'col-md-7':'col-md-12'}}">

                <div class="card">
                    <div class="card-header text-center">वडा</div>
                    <div class="card-body">
                        <table class="table table-bordered text-center  table-sm">
                            <thead>
                            <th>क्रं.स.</th>
                            <th>वडा नं.</th>
                            @if(Auth::User()->privilege != 'creator')
                                <th>कार्य</th>
                            @endif
                            </thead>
                            <tbody>
                            @foreach($wards as $ward)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$ward->ward_no}}</td>
                                    @if(Auth::User()->privilege != 'creator')
                                        <td>
                                            <a href="{{route('edit-ward',$ward->id)}}" class="btn btn-sm btn-success">
                                                <li class="fa fa-edit"></li>
                                            </a>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>

            </div>
        </div>

    </div>
@stop
