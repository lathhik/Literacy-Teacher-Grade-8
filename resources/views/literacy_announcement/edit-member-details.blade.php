@extends('layouts.literacy_announcement')
@section('content')
    @php

        $member_details = $house->members;
        $head = $member_details->where('relation_to_head', '=', 'self')->first();

        $member_details = $member_details->filter(function ($q) use ($head) {
            return $q->id != $head->id;
        });

        $grades = [
            'basic'=>'आधारभुत',
            'secondary'=>'माध्यमिक',
            'higher_education'=>'उच्च शिक्षा',
            'informal'=>'अनैपचारिक',
        ];


    @endphp
    <div class="container">
        <div class="mt-4">
            @include('messages.succFail')
        </div>
        <div class="row mt-4">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-center">घर विवरण थप्नुहोस</div>
                    <div class="card-body">
                        <form method="post" action="{{route('edit-member-details',$house->id)}}">
                            @csrf
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="formGroupExampleInput">वडा नं.:</label>
                                    <select name="ward_no" id="ward" class="form-control" required>
                                        <option value="" disabled selected>वडा नं छान्नुहोस.</option>
                                        @foreach($wards as $ward)
                                            <option
                                                value="{{$ward->id}}" {{($house->tole->ward->id == $ward->id)?'selected':''}}>{{$ward->ward_no}}</option>
                                            @if($house->tole->ward->id == $ward->id)
                                                @php($selected_ward = $ward)
                                            @endif
                                        @endforeach
                                    </select>
                                    @if($errors->has('ward_no'))
                                        <p class="text-danger">{{$errors->first('ward_no')}}</p>
                                    @endif
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="formGroupExampleInput">टोल:</label>
                                    <select name="tole" id="tole" class="form-control" required>
                                        @foreach($selected_ward->toles as $tole)
                                            <option
                                                value="{{$tole->id}}">{{$tole->tole}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('tole'))
                                        <p class="text-danger">{{$errors->first('tole')}}</p>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="formGroupExampleInput">घर नं.:</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput"
                                           placeholder="Input House No." name="house_no"
                                           value="{{$house->house_no}}">
                                    @if($errors->has('house_no'))
                                        <p class="text-danger">{{$errors->first('house_no')}}</p>
                                    @endif
                                </div>
                            </div>
                            <hr>
                            <h1 style="text-align: center">परिवारको सदस्यको विवरण</h1>
                            <h4>घर मुलिको विवरण</h4>

                            <div id="memberDetails">
                                <div id="headDetails">
                                    <div class="items">
                                        <hr>
                                        <div class="item-content">
                                            <input type="hidden" name="id[]" value="{{$head->id}}">
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label for="">पहिलो नाम:</label>
                                                    <input type="text" class="form-control" id="" name="first_name[]"
                                                           placeholder="Input First Name" value="{{$head->first_name}}"
                                                           required>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="">बिचको नाम:</label>
                                                    <input type="text" class="form-control" id="" name="middle_name[]"
                                                           placeholder="Input Middle Name" value="{{$head->middle_name}}">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="">थर:</label>
                                                    <input type="text" class="form-control" id="" name="last_name[]"
                                                           placeholder="Input Last Name" required
                                                           value="{{$head->last_name}}">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="">उमेर:</label>
                                                    <input type="text" class="form-control" id="" name="age[]"
                                                           placeholder="उमेर हाल्नुहोस" required value="{{$head->age}}">
                                                </div>

                                                <input type="hidden" value="self" name="rel_to_head[]">


                                                <div class="form-group col-md-4">
                                                    <label for="">लिङ्ग:</label><br>
                                                    <input type="checkbox" value="male" name="gender[]"
                                                           class="mt-2 chb" {{($head->gender=='male')?'checked':''}}>
                                                    <label for="male">
                                                        पुरुष </label>&nbsp;&nbsp;
                                                    <input type="checkbox" value="female" name="gender[]"
                                                           class="chb" {{($head->gender=='female')?'checked':''}}>
                                                    <label for="female">
                                                        महिला </label>&nbsp;
                                                    <input type="checkbox" value="other" name="gender[]"
                                                           class="chb" {{($head->gender=='other')?'checked':''}}>
                                                    <label for="other">
                                                        अन्य </label>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="">साक्षरता:</label><br>
                                                    <input type="checkbox" value="1" class="mt-2 lit_yes chb"
                                                           name="literacy[]" {{($head->literacy == 1)?'checked':''}}>
                                                    <label for="">
                                                        साक्षर </label>&nbsp;
                                                    <input type="checkbox" value="0" class="lit_no chb"
                                                           name="literacy[]" {{($head->literacy == 0)?'checked':''}}>
                                                    <label for="">
                                                        निरक्षर </label>&nbsp;
                                                </div>
                                                <div class="form-group col-md-4 grade" id=""
                                                     style="{{($head->literacy == 0)?'display: none':''}}">
                                                    <label for="formGroupExampleInput">तह:</label><br>
                                                    <select name="grade[]" id="" class="form-control">
                                                        <option value="" selected disabled>तह छान्नुहोस</option>
                                                        @foreach($grades as $key => $value )
                                                            <option
                                                                value="{{$key}}" {{($head->grade == $key)?'selected':''}}>{{$value}}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4 grade"  style="display: none">
                                                    <label for="">हाल  बिद्यालय गएको :</label><br>
                                                    <input type="checkbox" value="1" class="mt-2 chb" name="school_status[]" {{($head->school_status == 1)?'checked':''}}>
                                                    <label for="">छ </label>&nbsp;
                                                    <input type="checkbox" value="0" class="chb" name="school_status[]" {{($head->school_status == 0)?'checked':''}}>
                                                    <label for="">छैन </label>&nbsp;
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-8">
                                                    <label for="formGroupExampleInput">कैफियत:</label><br>
                                                    <textarea name="remarks[]" id="" cols="50"
                                                              rows="5">{{$head->remarks}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <h4>अन्य सदस्यको विवरण</h4>
                                @foreach($member_details as $member_detail)
                                    <div class="items">
                                        <hr>
                                        <input type="hidden" name="id[]" value="{{$member_detail->id}}">
                                        <div class="item-content">
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label for="">पहिलो नाम:</label>
                                                    <input type="text" class="form-control" id="" name="first_name[]"
                                                           placeholder="Input First Name"
                                                           value="{{$member_detail->first_name}}"
                                                           required>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="">बिचको नाम:</label>
                                                    <input type="text" class="form-control" id="" name="middle_name[]"
                                                           placeholder="Input Middle Name"
                                                           value="{{$member_detail->middle_name}}">
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label for="">थर:</label>
                                                    <input type="text" class="form-control" id="" name="last_name[]"
                                                           placeholder="Input Last Name"
                                                           required value="{{$member_detail->last_name}}">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="">उमेर:</label>
                                                    <input type="text" class="form-control" id="" name="age[]"
                                                           placeholder="उमेर हाल्नुहोस" required
                                                           value="{{$member_detail->age}}">
                                                </div>
                                                <div class="form-group col-md-4 head-detail">
                                                    <label for="formGroupExampleInput">घरमुलि संगको नाता</label><br>
                                                    <select name="rel_to_head[]" id="" class="form-control rel_to_head">
                                                        <option value="">नाता छान्नुहोस</option>
                                                        <option value="husband" {{($member_detail->relation_to_head == 'husband')?'selected':''}}>पति</option>
                                                        <option value="wife" {{($member_detail->relation_to_head == 'wife')?'selected':''}}>पत्नि</option>
                                                        <option value="father" {{($member_detail->relation_to_head == 'father')?'selected':''}}>बुवा</option>
                                                        <option value="mother" {{($member_detail->relation_to_head == 'mother')?'selected':''}}>आमा</option>
                                                        <option value="son" {{($member_detail->relation_to_head == 'son')?'selected':''}}>छोरा</option>
                                                        <option value="daughter" {{($member_detail->relation_to_head == 'daughter')?'selected':''}}>छोरी</option>
                                                        <option value="big-brother" {{($member_detail->relation_to_head == 'big-brother')?'selected':''}}>दाई</option>
                                                        <option value="small-brother" {{($member_detail->relation_to_head == 'small-brother')?'selected':''}}>भाई</option>
                                                        <option value="big-sister" {{($member_detail->relation_to_head == 'big-sister')?'selected':''}}>दिदि</option>
                                                        <option value="small-sister" {{($member_detail->relation_to_head == 'small-sister')?'selected':''}}>बहिनी</option>
                                                        <option value="granddaughter-in-law" {{($member_detail->relation_to_head == 'granddaughter-in-law')?'selected':''}}>नातिनी बुहारी</option>
                                                        <option value="vauju" {{($member_detail->relation_to_head == 'vauju')?'selected':''}}>भाउजु</option>
                                                        <option value="buhari" {{($member_detail->relation_to_head == 'buhari')?'selected':''}}>बुहारी</option>
                                                        <option value="grandson" {{($member_detail->relation_to_head == 'grandson')?'selected':''}}>नाति</option>
                                                        <option value="granddaughter" {{($member_detail->relation_to_head == 'granddaughter')?'selected':''}}>नातिनी</option>
                                                        <option value="vatij" {{($member_detail->relation_to_head == 'vatij')?'selected':''}}>भतिज</option>
                                                        <option value="vatiji" {{($member_detail->relation_to_head == 'vatiji')?'selected':''}}>भतिजी</option>
                                                        <option value="kaka" {{($member_detail->relation_to_head == 'kaka')?'selected':''}}>काका</option>
                                                        <option value="kaki" {{($member_detail->relation_to_head == 'kaki')?'selected':''}}>काकी</option>
                                                        <option value="son-in-law" {{($member_detail->relation_to_head == 'son-in-law')?'selected':''}}>ज्‌वाइ</option>
                                                        <option value="brother-in-law" {{($member_detail->relation_to_head == 'brother-in-law')?'selected':''}}>भिनाजु</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="">लिङग:</label><br>
                                                    <input type="checkbox" value="male" name="gender[]"
                                                           class="mt-2 chb" {{($member_detail->gender == 'male')?'checked':''}}>
                                                    <label for="male">
                                                        पुरुष </label>&nbsp;&nbsp;
                                                    <input type="checkbox" value="female" name="gender[]"
                                                           class="chb" {{($member_detail->gender == 'female')?'checked':''}}>
                                                    <label for="female">
                                                        महिला </label>&nbsp;
                                                    <input type="checkbox" value="other" name="gender[]"
                                                           class="chb"{{($member_detail->gender == 'other')?'checked':''}}>
                                                    <label for="other">
                                                        अन्य </label>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="">साक्षरता:</label><br>
                                                    <input type="checkbox" value="1" class="mt-2 lit_yes chb"
                                                           name="literacy[]"{{($member_detail->literacy == '1')?'checked':''}}>
                                                    <label for="">
                                                        साक्षर </label>&nbsp;
                                                    <input type="checkbox" value="0" class="lit_no chb"
                                                           name="literacy[]"{{($member_detail->literacy == '0')?'checked':''}}>
                                                    <label for="">
                                                        निरक्षर </label>&nbsp;
                                                </div>
                                                <div class="form-group col-md-4 grade" id=""
                                                     style="{{($member_detail->literacy == '0')?'display: none':''}}">
                                                    <label for="formGroupExampleInput">तह:</label><br>
                                                    <select name="grade[]" id="" class="form-control">
                                                        <option value="" selected disabled>तह छान्नुहोस</option>
                                                        @foreach($grades as $key => $value )
                                                            <option
                                                                value="{{$key}}" {{($member_detail->grade == $key)?'selected':''}}>{{$value}}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4 grade"  style="{{($member_detail->literacy == '0')?'display: none':''}}">
                                                    <label for="">हाल  बिद्यालय गएको :</label><br>
                                                    <input type="checkbox" value="1" class="mt-2 chb" name="school_status[]" {{($member_detail->school_status == 1)?'checked':''}}>
                                                    <label for="">छ </label>&nbsp;
                                                    <input type="checkbox" value="0" class="chb" name="school_status[]" {{($member_detail->school_status == 0)?'checked':''}}>
                                                    <label for="">छैन </label>&nbsp;
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-8">
                                                    <label for="formGroupExampleInput">कैफियत:</label><br>
                                                    <textarea name="remarks[]" id="" cols="50"
                                                              rows="5">{{$member_detail->remarks}}
                                                    </textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-danger remove-btn">हटाउनुहोस</button>
                                    </div>
                                @endforeach
                            </div>
                            <div class="mt-2">
                                <button type="button" class="btn btn-md btn-success" id="addRepeater">थप्नुहोस</button>
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-success float-right" type="submit">बुझाउनुहोस</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="repeater" style="display: none">
        <div class="items">
            <hr>
            <div class="item-content">
                <div class="row">
                    <div class="form-group col-md-4">
                        <label for="">पहिलको नाम:</label>
                        <input type="text" class="form-control" id="" name="first_name[]"
                               placeholder="Input First Name" value="" required>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="">बिचको नाम:</label>
                        <input type="text" class="form-control" id="" name="middle_name[]"
                               placeholder="Input Middle Name">
                    </div>

                    <div class="form-group col-md-4">
                        <label for="">थर:</label>
                        <input type="text" class="form-control" id="" name="last_name[]"
                               placeholder="Input Last Name" required>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="">उमेर:</label>
                        <input type="number" class="form-control" id="" name="age[]"
                               placeholder="उमेर हाल्नुहोस" required>
                    </div>
                    <div class="form-group col-md-4 head-detail">
                        <label for="formGroupExampleInput">घरमुलिसंगको नाता</label><br>
                        <select name="rel_to_head[]" id="" class="form-control rel_to_head" >
                            <option value="">नाता छान्नुहोस</option>
                            <option value="husband">पति</option>
                            <option value="wife">पत्नि</option>
                            <option value="father">बुवा</option>
                            <option value="mother">आमा</option>
                            <option value="son">छोरा</option>
                            <option value="daughter">छोरी</option>
                            <option value="big-brother">दाई</option>
                            <option value="small-brother">भाई</option>
                            <option value="big-sister">दिदि</option>
                            <option value="small-sister">बहिनी</option>
                            <option value="vauju">भाउजु</option>
                            <option value="buhari">बुहारी</option>
                            <option value="grandson">नाति</option>
                            <option value="granddaughter">नातिनी</option>
                            <option value="granddaughter-in-law">नातिनी बुहारी</option>
                            <option value="vatij">भतिज</option>
                            <option value="vatiji">भतिजी</option>
                            <option value="kaka">काका</option>
                            <option value="kaki">काकी</option>
                            <option value="son-in-law">ज्‌वाइ</option>
                            <option value="brother-in-law">भिनाजु</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="">लिङ्ग:</label><br>
                        <input type="checkbox" value="male" name="gender[]"
                               class="mt-2 chb checkboxRequired" required>
                        <label for="male">
                            पुरुष </label>&nbsp;&nbsp;
                        <input type="checkbox" value="female" name="gender[]" class="chb checkboxRequired" required>
                        <label for="female">
                            महिला </label>&nbsp;
                        <input type="checkbox" value="other" name="gender[]" class="chb checkboxRequired" required>
                        <label for="other">
                            अन्य </label>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="">साक्षरता:</label><br>
                        <input type="checkbox" value="1" class="mt-2 lit_yes chb checkboxRequired" name="literacy[]" required>
                        <label for="">
                            साक्षर </label>&nbsp;
                        <input type="checkbox" value="0" class="lit_no chb checkboxRequired" name="literacy[]" required>
                        <label for="">
                            निरक्षर </label>&nbsp;
                    </div>
                    <div class="form-group col-md-4 grade" id="" style="display: none">
                        <label for="formGroupExampleInput">तह:</label><br>
                        <select name="grade[]" id="" class="form-control">
                            <option value="select" selected >तह छान्नुहोस</option>
                            <option value="basic">आधारभुत</option>
                            <option value="secondary">माध्यमिक</option>
                            <option value="higher_education">उच्च शिक्षा</option>
                            <option value="bachelor">अनैपचारिक</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4 grade"  style="display: none">
                        <label for="">हाल  बिद्यालय गएको :</label><br>
                        <input type="checkbox" value="1" class="mt-2 chb" name="school_status[]" >
                        <label for="">छ </label>&nbsp;
                        <input type="checkbox" value="0" class="chb" name="school_status[]" >
                        <label for="">छैन </label>&nbsp;
                        <input type="checkbox" value="" class="chb" name="school_status[]" checked style="display: none">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-8">
                        <label for="formGroupExampleInput">कैफियत:</label><br>
                        <textarea name="remarks[]" id="" cols="50" rows="5"></textarea>
                    </div>
                </div>
            </div>
            <button type="button" class="btn btn-danger remove-btn">Remove</button>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            // repeatMemberDetailsWitoutRemove();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $('#ward').on('change', function () {
                var wardId = $(this).val();

                var ajxRoute = '{{route('get-tole-ward')}}';
                $.ajax({
                    url: ajxRoute,
                    type: 'POST',
                    data: {_token: CSRF_TOKEN, id: wardId},
                    dataType: 'JSON',
                    success: function (data) {
                        $('#tole').empty();
                        $('#tole').append('<option value="" disabled selected>Select Tole</option>');
                        $.each(data, function (key, value) {
                            $('#tole').append('<option value="' + value.id + '">' + value.tole + '</option>')
                        })
                    }
                })
            });

            $('#addRepeater ').on('click', function () {
                repeatMemberDetails();
            });
            $('#memberDetails').on('change','.checkboxRequired',function () {
                $(this).siblings('input').removeAttr('required');
            });
            $('#memberDetails').on('click', '.remove-btn', function () {
                $(this).parent().remove();
            });
            $('#memberDetails').on('click', '.lit_yes', function () {
                console.log('literate');
                $(this).parent().parent().parent().find('.grade').show();
            });
            $('#memberDetails').on('click', '.lit_no', function () {
                console.log('illiterate');
                $(this).parent().parent().parent().find('.grade').hide();
            });
            $('#memberDetails').on('change', '.chb', function () {
                $(this).parent().find(".chb").prop('checked', false);
                $(this).prop('checked', true);
            });
            $('#memberDetails').on('change', 'select.rel_to_head', function () {
                console.log('khjasd');
                var relation = $(this).val();
                let female = ['wife', 'mother', 'daughter', 'big-sister', 'small-sister', 'vauju', 'buhari', 'granddaughter', 'granddaughter-in-law', 'vatiji', 'kaki'];
                if(relation){
                    if(female.includes(relation)){
                        let femaleInput = $(this).closest('.row').find('input[value="female"]');
                        femaleInput.parent().find(".chb").prop('checked', false);
                        femaleInput.prop('checked',true);
                        femaleInput.siblings('input').removeAttr('required');

                    }else{
                        let maleInput = $(this).closest('.row').find('input[value="male"]');
                        maleInput.parent().find(".chb").prop('checked', false);
                        maleInput.prop('checked',true);
                        maleInput.siblings('input').removeAttr('required');

                    }
                }else{
                    let blankInput = $(this).closest('.row').find('input[value="female"]');
                    blankInput.parent().find(".chb").prop('checked', false);
                }

            });
            function repeatMemberDetails() {
                var repeater = $('#repeater .items').clone();
                $('#memberDetails').append(repeater);
            }

            function repeatMemberDetailsWitoutRemove() {
                var repeater = $('#repeater .items').clone();
                repeater.find(".remove-btn").remove();
                repeater.find(".head-detail").remove();
                repeater.append('<input name="rel_to_head[]" style="display:none;" value="self">');
                $('#headDetails').append(repeater);
            }

        });
    </script>
@stop
