@extends('layouts.literacy_announcement')
@section('style')
    <style>
        @page {
            size: A4;
            margin: 0;
        }

        @media print {
            html, body {
                width: 210mm;
                height: 297mm;
            }

            .print-body {
                margin-top: 100px;
                margin-left: 10%;
                margin-right: 10%;
            }

            .card {
                border: hidden;
            }
        }
    </style>
@stop

@section('content')
    <div class="container">
        <div class="d-print-none">
            <h1>Reports</h1>
        </div>
        <div class="row">
            <div class="col-md-3 d-print-none">
                <form action="" method="post" id="filterForm" >
                    @csrf
                    <div class="form-group">
                        <label for="ward">वडा</label>
                        <select name="ward" id="ward" class="form-control">
                            <option value="all" selected>सबै</option>
                            @foreach($wards as $ward)
                                <option
                                    value="{{$ward->id}}" {{$selectedVars['ward']==$ward->id ? 'selected':''}}>{{$ward->ward_no}}</option>
                                @if($selectedVars['ward']==$ward->id )
                                    @php($selectedWard = $ward)
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group" id="toleInput" style="display: {{isset($selectedWard)?'block':'none'}}">
                        <label for="tole">Tole</label>
                        <select name="tole" id="tole" class="form-control">
                            @if(isset($selectedWard))
                                <option value="all" selected>सबै</option>
                                @foreach($selectedWard->toles as $tole)
                                    <option
                                        value="{{$tole->id}}" {{$selectedVars['tole']==$tole->id ? 'selected':''}}>{{$tole->tole}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="filter_by">फिल्टर</label>
                        <select name="filter_by" id="filter_by" class="form-control">
                            <option value="gender" {{$filter == 'gender'?'selected':''}}>लिङ्ग</option>
                            <option value="age_group" {{$filter == 'age_group'?'selected':''}}>उमेर समूह</option>
                            <option value="school_status" {{$filter == 'school_status'?'selected':''}}>विद्यालय
                                गएको/नगएको
                            </option>
                        </select>
                    </div>
                    <div class="row " id="defineAgeGroup" style="display: {{isset($filteredData['ageFrom'])?'flex':'none'}}">
                        <div class="col-md-5 form-group">
                            <input name="ageFrom" id="ageFrom" type="text" class="form-control"
                                   value=" {{$filteredData['ageFrom']?? null}}">
                        </div>
                        <div class="col-md-2">
                            देखी
                        </div>
                        <div class="col-md-5 form-group">
                            <input name="ageTo" id="ageTo" type="text" class="form-control"
                                   value=" {{$filteredData['ageTo']?? null}}">
                        </div>
                    </div>
                    <button class="btn btn-success" id="filterButton">फिल्टर</button>
                </form>
            </div>

            <div class=" col-md-8 card print-body">
                <div class="card-body row">
                    <div class="col-md-12 text-center">
                        <h4>चन्द्रागिरी नगरपालिका </h4>
                        <h4>साक्षर घोषणा अभियान - २०७६ </h4>
                        <h4>निरक्षरताको नामनामेसी संकलन घरधुरी सर्वेक्षण फारम - २०७६</h4>

                    </div>
                    <div class="col-md-12 row my-4">
                        <div class="col-md-6">वडा नं:</div>
                        <div class="col-md-6 text-right">गाउँ टोल:</div>
                    </div>

                    <table class="table table-bordered text-center">
                        <thead>
                        <tr class="text-center">
                            <td rowspan="2">क्रं.स.</td>
                            @if($filter =='gender')
                                <td colspan="2">लिङ्ग</td>
                            @elseif($filter=='age_group')
                                <td colspan="2">({{$filteredData['ageFrom']}}-{{$filteredData['ageTo']}}) बर्ष</td>
                            @elseif($filter=='school_status')
                                <td colspan="2">(०-५) बर्ष</td>
                                <td colspan="2">(५-१८) बर्ष</td>
                            @endif
                            <td rowspan="2">कुल साक्षर/निरक्षर</td>
                        </tr>
                        <tr>
                            <td>पुरुष</td>
                            <td>महिला</td>
                            @if($filter=='school_status')
                                <td>पुरुष</td>
                                <td>महिला</td>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{($filter =='school_status')?'विद्यालय गएको':'साक्षर'}}</td>
                            @if($filter =='gender')
                                <td>{{$filteredData['literate_male']}}</td>
                                <td>{{$filteredData['literate_female']}}</td>
                                <td>{{$filteredData['literate_male']+$filteredData['literate_female']}}</td>
                            @elseif($filter=='age_group')
                                <td>{{$filteredData['literate_male']}}</td>
                                <td>{{$filteredData['literate_female']}}</td>
                                <td>{{$filteredData['literate_male']+$filteredData['literate_female']}}</td>
                            @elseif($filter == 'school_status')
                                <td>{{$filteredData['school_status_male_yes']}}</td>
                                <td>{{$filteredData['school_status_female_yes']}}</td>
                                <td>{{$filteredData['school_status_male2_yes']}}</td>
                                <td>{{$filteredData['school_status_female2_yes']}}</td>
                                <td>{{$filteredData['school_status_male_yes']+$filteredData['school_status_female_yes']+
                                $filteredData['school_status_male2_yes']+$filteredData['school_status_female2_yes']}}</td>
                            @endif
                        </tr>
                        <tr>
                            <td>{{($filter =='school_status')?'विद्यालय नगएको':'निरक्षर'}}</td>
                            @if($filter =='gender')
                                <td>{{$filteredData['illiterate_male']}}</td>
                                <td>{{$filteredData['illiterate_female']}}</td>
                                <td>{{$total_illiterate=$filteredData['illiterate_male']+$filteredData['illiterate_female']}}</td>
                            @elseif($filter=='age_group')
                                <td>{{$filteredData['illiterate_male']}}</td>
                                <td>{{$filteredData['illiterate_female']}}</td>
                                <td>{{$filteredData['illiterate_male']+$filteredData['illiterate_female']}}</td>
                            @elseif($filter == 'school_status')
                                <td>{{$filteredData['school_status_male_no']}}</td>
                                <td>{{$filteredData['school_status_female_no']}}</td>
                                <td>{{$filteredData['school_status_male2_no']}}</td>
                                <td>{{$filteredData['school_status_female2_no']}}</td>
                                <td>{{$filteredData['school_status_male_no']+$filteredData['school_status_female_no']+
                                $filteredData['school_status_male2_no']+$filteredData['school_status_female2_no']}}</td>
                            @endif
                        </tr>
                        <tr>
                            <td>कुल जनसंख्या</td>
                            @if($filter =='gender')
                                <td>{{$total_male=$filteredData['illiterate_male']+$filteredData['literate_male']}}</td>
                                <td>{{$total_female = $filteredData['illiterate_female']+$filteredData['literate_female']}}</td>
                                <td>{{$total_male+$total_female}}</td>
                            @elseif($filter=='age_group')
                                <td>{{$total_male = $filteredData['illiterate_male']+$filteredData['literate_male']}}</td>
                                <td>{{$total_female = $filteredData['illiterate_female']+$filteredData['literate_female']}}</td>
                                <td>{{$total_male+$total_female}}</td>
                            @elseif($filter == 'school_status')
                                <td>{{$total_male=$filteredData['school_status_male_no']+$filteredData['school_status_male_yes']}}</td>
                                <td>{{$total_female=$filteredData['school_status_female_no']+$filteredData['school_status_female_yes']}}</td>
                                <td>{{$total_male2=$filteredData['school_status_male2_no']+$filteredData['school_status_male2_yes']}}</td>
                                <td>{{$total_female2=$filteredData['school_status_female2_no']+$filteredData['school_status_female2_yes']}}</td>
                                <td>{{$total_male+$total_female+$total_male2+$total_female2}}</td>
                            @endif
                        </tr>
                        </tbody>
                    </table>
                    <button class="mx-auto btn btn-primary btn-sm d-print-none" onclick="window.print()">प्रिन्ट
                        गर्नुहोस
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $('#ward').on('change', function () {
                var wardId = $(this).val();
                if (wardId == 'all') {
                    $('#toleInput').hide();
                } else {
                    $('#toleInput').show();
                    var ajxRoute = '{{route('get-tole-ward')}}';
                    $.ajax({
                        url: ajxRoute,
                        type: 'POST',
                        data: {_token: CSRF_TOKEN, id: wardId},
                        dataType: 'JSON',
                        success: function (data) {
                            $('#tole').empty();
                            $('#tole').append('<option value="all" selected>सबै</option>');
                            $.each(data, function (key, value) {
                                $('#tole').append('<option value="' + value.id + '">' + value.tole + '</option>')
                            })
                        }
                    })
                }

            });

            $('#filter_by').on('change', function () {

                if ($('#filter_by').val()==='age_group') {
                    $("#defineAgeGroup").show();
                }else{
                    $("#defineAgeGroup").hide();
                }

            });
            $('#filterButton').on('click', function (event) {
                event.preventDefault();
                if(parseInt($('#ageFrom').val())>parseInt($('#ageTo').val())){
                    alert('उमेर समुह क्रमस सानो देखि ठुलो मिलाएर लेख्नुहोस');
                }else{
                    $('#filterForm').submit();
                }
            });
        });
    </script>
@stop
