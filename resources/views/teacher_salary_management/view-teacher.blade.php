@extends('layouts.teacher_salary_management')
@section('content')
    @php
        $levels = [
        'secondary'=>'माध्यमिक',
        'lower_secondary'=>'निम्न माध्यमिक',
        'primary_slc_pass'=>'प्राथमिक(एस.एल.सि उतीर्ण)',
        'primary_slc_pass_two'=>'प्राथमिक(दुई बिषयसम्म एस.एल.सि अनुतीर्ण)',
        'primary_slc_fail_more_two'=>'प्राथमिक(दुई बिषयभन्दा बढी एस.एल.सि अनुतीर्ण)'
        ];
    $classes = [
        'FC'=>'प्रथम श्रेणी',
        'SC'=>'द्धितीय श्रेणी',
        'TC'=>'तृतीय श्रेणी',

    ];
        $grades = [
            '1'=>'१',
            '2'=>'२',
            '3'=>'३',
            '4'=>'४',
            '5'=>'५',
            '6'=>'६',
            '7'=>'७',
            '8'=>'८',
            '9'=>'९',
            '10'=>'१०',
        ]

    @endphp

    <div class="container">
        <div class="mt-4">
            @include('messages.succFail')
        </div>
        <div class="row mt-4">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-center">शिक्षक विबरण</div>
                    <div class="card-body">
                        <table class="table table-bordered text-center  table-sm">
                            <thead>
                            <th>क्र.सं</th>
                            <th>पुरा नामथर</th>
                            <th>तह</th>
                            <th>श्रेणी</th>
                            <th>ग्रेड संख्या</th>
                            <th>ग्रेड दर</th>
                            <th>शुरु तलब स्केल</th>
                            <th>जम्मा</th>
                            <th>कार्य</th>
                            </thead>
                            <tbody>
                            @foreach($teachers as $teacher)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$teacher->first_name}} {{$teacher->middle_name}} {{$teacher->last_name}}</td>
                                    <td>
                                        @foreach($levels as $key=>$value)
                                            @if($teacher->class->level == $key)
                                                {{$value}}
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($classes as $key => $value)
                                            @if($teacher->class->class == $key)
                                                {{$value}}
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($grades as $key => $value)
                                            @if($teacher->grade->grade_value == $key)
                                                {{$value}}
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        {{number_format($teacher->class->salary_scale/30)}}</td>
                                    <td>
                                        {{number_format($teacher->class->salary_scale)}}
                                    </td>
                                    <td>
                                        @php
                                            $grade_rate = $teacher->class->salary_scale/30;
                                            $grade = $teacher->grade->grade_value;
                                            $total = ($grade_rate * $grade) + $teacher->class->salary_scale;
                                        @endphp
                                        {{number_format($total)}}
                                    </td>
                                    <td>
                                        <a class="btn btn-sm btn-success"
                                           href="{{route('edit-teacher',$teacher->id)}}"><i
                                                class="fa fa-edit"></i></a>
                                        <a class="btn btn-sm btn-danger"
                                           href="{{route('delete-teacher',$teacher->id)}}"><i
                                                class="fa fa-trash"></i></a>
                                        <a href="{{route('view-teacher-details',$teacher->id)}}"
                                           class="btn btn-sm btn-primary">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>

            </div>
        </div>

    </div>
@stop

