@extends('layouts.teacher_salary_management')
@section('content')
    @php
        $levels = [
        'secondary'=>'माध्यमिक',
        'lower_secondary'=>'निम्न माध्यमिक',
        'primary_slc_pass'=>'प्राथमिक(एस.एल.सि उतीर्ण)',
        'primary_slc_pass_two'=>'प्राथमिक(दुई बिषयसम्म एस.एल.सि अनुतीर्ण)',
        'primary_slc_fail_more_two'=>'प्राथमिक(दुई बिषयभन्दा बढी एस.एल.सि अनुतीर्ण)'
        ];
    $classes = [
        'FC'=>'प्रथम श्रेणी',
        'SC'=>'द्धितीय श्रेणी',
        'TC'=>'तृतीय श्रेणी',

    ];
        $grades = [
            '1'=>'१',
            '2'=>'२',
            '3'=>'३',
            '4'=>'४',
            '5'=>'५',
            '6'=>'६',
            '7'=>'७',
            '8'=>'८',
            '9'=>'९',
            '10'=>'१०',
        ]

    @endphp
    <div class="container-fluid">
        <div class="mt-4">
            @include('messages.succFail')
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header text-center bg-primary">आधारभूत जानकारी</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4 mx-auto">
                                <div style="border: 1px solid red;">
                                    <img src="{{asset('teachers_files/'.((!empty($image))?$image->file:''))}}"
                                         alt="photo" class="img-fluid" height="100px">
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="col-md-12 mt-1" style="">
                            <div style="height:auto;line-height: 30px" class="">
                                <li style="list-style:none" class=""><b>पहिलो नाम :</b> {{$teacher->first_name}}</li>
                                <li style="list-style:none"><b>बीचको नाम :</b> {{$teacher->middle_name}}</li>
                                <li style="list-style:none"><b>थर :</b> {{$teacher->last_name}}</li>
                                <li style="list-style:none"><b>स्थाई ठेगाना :</b> {{$teacher->permanent_address}}</li>
                                <li style="list-style:none"><b>अस्थायी ठेगाना :</b> {{$teacher->temporary_address}}</li>
                                <li style="list-style:none"><b>जन्म मिति :</b> {{$teacher->dob}}</li>
                                <li style="list-style:none"><b>ईमेल :</b> {{$teacher->email}}</li>
                                <li style="list-style:none"><b>फोन नं :</b> {{$teacher->phone}}</li>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header text-center">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab"
                                   href="#home" role="tab" aria-controls="home" aria-selected="true">
                                    अन्य जानकारी
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab"
                                   href="#profile" role="tab" aria-controls="profile"
                                   aria-selected="false">
                                    फाईलहरू
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <div class="card">
                                    <div class="card-body">
                                        <table>
                                            <tr>
                                                <td><b>तह :</b></td>
                                                @foreach($levels as $key => $value)
                                                    <td>
                                                        @if($teacher->class->level == $key)
                                                            {{$value}}
                                                        @endif
                                                    </td>
                                                @endforeach
                                            </tr>
                                            <tr>
                                                <td><b>पद/श्रेणी :</b></td>
                                                @foreach($classes as $key => $value)
                                                    <td>
                                                        @if($teacher->class->class == $key)
                                                            {{$value}}
                                                        @endif
                                                    </td>
                                                @endforeach
                                            </tr>
                                            <tr>
                                                <td><b>ग्रेड संख्या :</b></td>
                                                @foreach($grades as $key => $value)
                                                    <td>
                                                        @if($teacher->grade->grade_value == $key)
                                                            {{$value}}
                                                        @endif
                                                    </td>
                                                @endforeach
                                            </tr>
                                            <tr>
                                                <td><b>शुरु तलब स्केल :</b></td>
                                                <td>रु {{number_format($teacher->class->salary_scale)}}</td>
                                            </tr>
                                            <tr>
                                                <th>ग्रेड दर :</th>
                                                <td>रु {{number_format($teacher->class->salary_scale/30)}}</td>
                                            </tr>
                                            <tr>
                                                <th>जम्मा :</th>

                                                <td>
                                                    @php
                                                        $grade_rate = $teacher->class->salary_scale/30;
                                                        $grade = $teacher->grade->grade_value;
                                                        $total = ($grade_rate * $grade) + $teacher->class->salary_scale;
                                                    @endphp
                                                    रु {{number_format($total)}}
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                <div class="card-group">
                                    @foreach($teacher_files as $file)
                                        <div class="col-sm-6">
                                            <div class="card mt-4" style="">
                                                <img src="{{asset('teachers_files/'.$file->file)}}"
                                                     alt="img" height="350" class="card-img-top">
                                                <div class="card-footer text-center">
                                                    <button class="btn btn-primary"><a
                                                            style="text-decoration: none;color: white"
                                                            href="{{route('download-file',$file->id)}}">Download</a>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
@stop

