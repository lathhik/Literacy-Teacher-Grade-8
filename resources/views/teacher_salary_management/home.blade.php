@extends('layouts.teacher_salary_management')
@section('style')
    {{--    <link rel="stylesheet" type="text/css" href="{{asset('public/css/print.css')}}" media="print"/>--}}
@stop

@section('content')

    <div class="container">
        <div class="row mt-4">
            <div class="col-md-3 d-print-none mb-3">
                <div class="card">
                    <div class="card-header text-center">कुल शिक्षक संख्या
                    </div>
                    <div class="card-body text-center">
                        <h3>{{count($teachers)}}</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-3 d-print-none mb-3">
                <div class="card">
                    <div class="card-header text-center">कुल शिक्षक संख्या(माध्यमिक)
                    </div>
                    <div class="card-body text-center">
                        <h3>
                            {{$teachers_secondary}}
                        </h3>
                    </div>
                </div>
            </div>
            <div class="col-md-3 d-print-none mb-3">
                <div class="card">
                    <div class="card-header text-center">कुल शिक्षक संख्या(निम्न माध्यमिक)
                    </div>
                    <div class="card-body text-center">
                        <h3>
                            {{$teachers_lower_secondary}}
                        </h3>
                    </div>
                </div>
            </div>
            <div class="col-md-3 d-print-none mb-3">
                <div class="card">
                    <div class="card-header text-center">कुल शिक्षक संख्या(प्राथमिक)
                    </div>
                    <div class="card-body text-center">
                        <h3></h3>
                    </div>
                </div>
            </div>
            <div class="col-md-3 d-print-none mb-3">
                <div class="card">
                    <div class="card-header text-center">कुल पुरुष प्रतिशत
                    </div>
                    <div class="card-body text-center">
                        <h3></h3>
                    </div>
                </div>
            </div>
            <div class="col-md-3 d-print-none mb-3">
                <div class="card">
                    <div class="card-header text-center">कुल महिला प्रतिशत
                    </div>
                    <div class="card-body text-center">
                        <h3></h3>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
