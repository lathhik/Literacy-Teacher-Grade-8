@extends('layouts.teacher_salary_management')
@section('content')

    @php
        $grades = [
            '1'=>'१',
            '2'=>'२',
            '3'=>'३',
            '4'=>'४',
            '5'=>'५',
            '6'=>'६',
            '7'=>'७',
            '8'=>'८',
            '9'=>'९',
            '10'=>'१०',
        ]

    @endphp

    <div class="container">
        <div class="mt-4">
            @include('messages.succFail')
        </div>
        <div class="row mt-4">
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header text-center">ग्रेड थप्नुहोस</div>
                    <div class="card-body">
                        <form method="post" action="{{route('add-tsm_grade-action')}}">
                            @csrf
                            <div class="form-group">
                                <label for="formGroupExampleInput">ग्रेड संख्या:</label>
                                <select name="grade" id="" class="form-control">
                                    <option value="" selected disabled>ग्रेड संख्या छान्नुहोस</option>
                                    <option value="1">१</option>
                                    <option value="2">२</option>
                                    <option value="3">३</option>
                                    <option value="4">४</option>
                                    <option value="5">५</option>
                                    <option value="6">६</option>
                                    <option value="7">७</option>
                                    <option value="8">८</option>
                                    <option value="9">९</option>
                                    <option value="10">१०</option>
                                </select>
                                @if($errors->has('grade'))
                                    <p class="text-danger">{{$errors->first('grade')}}</p>
                                @endif
                            </div>
                            <div>
                                <button class="btn btn-success float-right" type="submit">बुझाउनुहोस</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header text-center">ग्रेड बिबरण</div>
                    <div class="card-body">
                        <table class="table table-bordered text-center  table-sm">
                            <thead>
                            <th>क्रं.स.</th>
                            <th>ग्रेड संख्या</th>
                            <th>कार्य</th>
                            </thead>
                            <tbody>
                            @foreach($tsm_grades as $tsm_grade)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>
                                    @foreach($grades as $key => $value)
                                        @if($tsm_grade->grade_value == $key)
                                            {{$value}}
                                        @endif
                                    @endforeach
                                    <td>
                                        <a href="{{route('edit-tsm_grade',$tsm_grade->id)}}"
                                           class="btn btn-sm btn-success">
                                            <li class="fa fa-edit"></li>
                                        </a>
                                        <a href="{{route('delete-tsm_grade',$tsm_grade->id)}}"
                                           class="btn btn-sm btn-danger">
                                            <li class="fa fa-trash"></li>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>

            </div>
        </div>

    </div>
@stop
