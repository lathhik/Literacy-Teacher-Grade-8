@extends('layouts.teacher_salary_management')
@section('content')

    @php
        $grades = [
            '1'=>'१',
            '2'=>'२',
            '3'=>'३',
            '4'=>'४',
            '5'=>'५',
            '6'=>'६',
            '7'=>'७',
            '8'=>'८',
            '9'=>'९',
            '10'=>'१०',
              ]

    @endphp

    <div class="container">
        <div class="mt-4">
            @include('messages.succFail')
        </div>
        <div class="row mt-4 ml-5">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header text-center">ग्रेड सम्पादन</div>
                    <div class="card-body">
                        <form method="post" action="{{route('edit-tsm_grade-action',$tsm_grade->id)}}">
                            @csrf
                            <div class="form-group">
                                <label for="formGroupExampleInput">ग्रेड संख्या:</label>
                                <select name="grade" id="" class="form-control">
                                    <option value="" selected disabled>ग्रेड संख्या छान्नुहोस</option>
                                    <option value="1" {{($tsm_grade->grade_value == 1)?'selected':''}} >१</option>
                                    <option value="2" {{($tsm_grade->grade_value == 2)?'selected':''}}>२</option>
                                    <option value="3" {{($tsm_grade->grade_value == 3)?'selected':''}}>३</option>
                                    <option value="4" {{($tsm_grade->grade_value == 4)?'selected':''}}>४</option>
                                    <option value="5" {{($tsm_grade->grade_value == 5)?'selected':''}}>५</option>
                                    <option value="6" {{($tsm_grade->grade_value == 6)?'selected':''}}>६</option>
                                    <option value="7" {{($tsm_grade->grade_value == 7)?'selected':''}}>७</option>
                                    <option value="8" {{($tsm_grade->grade_value == 8)?'selected':''}}>८</option>
                                    <option value="9" {{($tsm_grade->grade_value == 9)?'selected':''}}>९</option>
                                    <option value="10" {{($tsm_grade->grade_value == 10)?'selected':''}}>१०</option>
                                </select>
                                @if($errors->has('grade'))
                                    <p class="text-danger">{{$errors->first('grade')}}</p>
                                @endif
                            </div>
                            <div>
                                <button class="btn btn-success float-right" type="submit">बुझाउनुहोस</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>
@stop
