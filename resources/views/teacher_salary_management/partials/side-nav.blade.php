<!-- Sidebar -->
<div class="border-right d-print-none" id="sidebar-wrapper">
    <div class="sidebar-heading">शिक्षक तलब ब्याबस्थापन</div>
    <div class="list-group list-group-flush">
        {{--        @if(Auth::user()->privilege == 'super_admin')--}}
        <a href="{{route('tsm-index')}}"
           class="list-group-item list-group-item-action {{ request()->is('teacher_salary_management') ? 'active' : '' }}">ड्यासबोर्ड</a>
        {{--        @endif--}}

        <div class="text-secondary list-group-item"><h4><strong>शिक्षक</strong></h4></div>
        <a href="{{route('add-school')}}"
           class="list-group-item list-group-item-action {{ Request::is('teacher_salary_management/add-school') ? 'active' : '' }}">
            विद्यालय</a>

        <a href="{{route('add-subject')}}"
           class="list-group-item list-group-item-action {{ Request::is('teacher_salary_management/add-subject') ? 'active' : '' }}">
            विषय</a>

        <a href="{{route('add-tsm_class')}}"
           class="list-group-item list-group-item-action {{ Request::is('teacher_salary_management/add-tsm_class') ? 'active' : '' }}">
            पद/श्रेणी</a>
        {{--        @endif--}}

        <a href="{{route('add-tsm_grade')}}"
           class="list-group-item list-group-item-action {{ Request::is('teacher_salary_management/add-tsm_grade') ? 'active' : '' }}">
            ग्रेड संख्या</a>

        <a href="{{route('add-teacher')}}"
           class="list-group-item list-group-item-action {{ Request::is('teacher_salary_management/add-teacher') ? 'active' : '' }}">
            शिक्षक थप्नुहोस</a>
        <a href="{{route('view-teacher')}}"
           class="list-group-item list-group-item-action {{ Request::is('teacher_salary_management/view-teacher') ? 'active' : '' }}">
            शिक्षक बिवरण</a>

        <div class="text-secondary list-group-item"><h4><strong>प्रशासन</strong></h4></div>

        <a href="{{route('add-staff-position')}}"
           class="list-group-item list-group-item-action  {{ Request::is('teacher_salary_management/add-staff-position') ? 'active' : '' }}">पद</a>

        <a href="{{route('add-staff')}}"
           class="list-group-item list-group-item-action  {{ Request::is('teacher_salary_management/add-staff') ? 'active' : '' }}">कर्मचारि</a>


        <a href=""
           class="list-group-item list-group-item-action  {{ Request::is('teacher_salary_management/view-reports') ? 'active' : '' }}">रिपोर्टहरू</a>
        <a href="{{route('add-tsm_admin')}}"
           class="list-group-item list-group-item-action {{ Request::is('teacher_salary_management/add-tsm_admin') ? 'active' : '' }}">प्रयोगकर्ता</a>
        {{--        @endif--}}
    </div>
</div>
<!-- /#sidebar-wrapper -->
