@extends('layouts.teacher_salary_management')
@section('content')
    @php
        $levels = [
        'secondary'=>'माध्यमिक',
        'lower_secondary'=>'निम्न माध्यमिक',
        'primary_slc_pass'=>'प्राथमिक(एस.एल.सि उतीर्ण)',
        'primary_slc_pass_two'=>'प्राथमिक(दुई बिषयसम्म एस.एल.सि अनुतीर्ण)',
        'primary_slc_fail_more_two'=>'प्राथमिक(दुई बिषयभन्दा बढी एस.एल.सि अनुतीर्ण)'
        ];
    $classes = [
        'FC'=>'प्रथम श्रेणी',
        'SC'=>'द्धितीय श्रेणी',
        'TC'=>'तृतीय श्रेणी',

    ];
        $grades = [
            '1'=>'१',
            '2'=>'२',
            '3'=>'३',
            '4'=>'४',
            '5'=>'५',
            '6'=>'६',
            '7'=>'७',
            '8'=>'८',
            '9'=>'९',
            '10'=>'१०',
        ];
    $school_levels =[

    ]

    @endphp

    <div class="container">
        <div class="mt-4">
            @include('messages.succFail')
        </div>
        <div class="row mt-4">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-center">शिक्षक सम्मपादन</div>
                    <div class="card-body">
                        <form method="post" action="{{route('edit-teacher-action',$teacher->id)}}"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="formGroupExampleInput">नाम:</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput"
                                           placeholder="नाम" name="first_name" value="{{$teacher->first_name}}">
                                    @if($errors->has('first_name'))
                                        <p class="text-danger">{{$errors->first('first_name')}}</p>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="formGroupExampleInput">बिचको नाम:</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput"
                                           placeholder="बिचको नाम" name="middle_name" value="{{$teacher->middle_name}}">
                                    @if($errors->has('middle_name'))
                                        <p class="text-danger">{{$errors->first('middle_name')}}</p>
                                    @endif
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="formGroupExampleInput">थर:</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput"
                                           placeholder="थर" name="last_name" value="{{$teacher->last_name}}">
                                    @if($errors->has('last_name'))
                                        <p class="text-danger">{{$errors->first('last_name')}}</p>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="formGroupExampleInput">स्थायी ठेगाना:</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput"
                                           placeholder="स्थायी ठेगाना" name="permanent_address"
                                           value="{{$teacher->permanent_address}}">
                                    @if($errors->has('permanent_address'))
                                        <p class="text-danger">{{$errors->first('permanent_address')}}</p>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="formGroupExampleInput">अस्थायी ठेगाना</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput"
                                           placeholder="अस्थायी ठेगाना" name="temporary_address"
                                           value="{{$teacher->temporary_address}}">
                                    @if($errors->has('temporary_address'))
                                        <p class="text-danger">{{$errors->first('temporary_address')}}</p>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="formGroupExampleInput">जन्म मिति:</label>
                                    <input type="date" class="form-control" id="formGroupExampleInput"
                                           placeholder="अस्थायी ठेगाना" name="date_of_birth"
                                           value="{{$teacher->dob}}">
                                    @if($errors->has('date_of_birth'))
                                        <p class="text-danger">{{$errors->first('date_of_birth')}}</p>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="formGroupExampleInput">ईमेल:</label>
                                    <input type="email" class="form-control" id="formGroupExampleInput"
                                           placeholder="ईमेल" name="email" value="{{$teacher->email}}">
                                    @if($errors->has('email'))
                                        <p class="text-danger">{{$errors->first('email')}}</p>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="formGroupExampleInput">फोन:</label>
                                    <input type="number" class="form-control" id="formGroupExampleInput"
                                           placeholder="फोन" name="phone" value="{{$teacher->phone}}">
                                    @if($errors->has('phone'))
                                        <p class="text-danger">{{$errors->first('phone')}}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="designation_date">नियुक्ति मिति:</label>
                                    <input type="date" class="form-control" id="start_date"
                                           placeholder="फोन" name="designation_date"
                                           value="{{$teacher->designation_date}}">
                                    @if($errors->has('designation_date'))
                                        <p class="text-danger">{{$errors->first('designation_date')}}</p>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="school">विद्यालय:</label>
                                    <select name="school" id="school" class="form-control">
                                        <option value="" selected disabled>विद्यालय छान्नुहोस</option>
                                        @foreach($schools as $school)
                                            <option
                                                value="{{$school->id}}" {{$teacher->school->school_name == $school->school_name?'selected':''}}>
                                                {{$school->school_name}}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('school'))
                                        <p class="text-danger">{{$errors->first('school')}}</p>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="subject">विषय:</label>
                                    <select name="subject" id="subject" class="form-control">
                                        <option value="" selected disabled>विषय छान्नुहोस</option>
                                        @foreach($subjects as $subject)
                                            <option
                                                value="{{$subject->id}}" {{$teacher->subject->subject_name == $subject->subject_name?'selected':''}}>
                                                {{$subject->subject_name}}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('subject'))
                                        <p class="text-danger">{{$errors->first('subject')}}</p>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="formGroupExampleInput">ग्रेड:</label>
                                    <select name="grade" id="" class="form-control">
                                        <option value="" selected disabled>ग्रेड छान्नुहोस</option>
                                        @foreach($tsm_grades as $tsm_grade)
                                            <option
                                                value="{{$tsm_grade->id}}" {{$teacher->grade->grade_value == $tsm_grade->grade_value?'selected':''}}>
                                                @foreach($grades as $key => $value)
                                                    @if($tsm_grade->grade_value == $key)
                                                        {{$value}}
                                                    @endif
                                                @endforeach
                                            </option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('grade'))
                                        <p class="text-danger">{{$errors->first('grade')}}</p>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="formGroupExampleInput">पद/श्रेणी छान्नुहोस:</label>
                                    <select name="class" id="" class="form-control">
                                        <option value="" selected disabled>पद/श्रेणी छान्नुहोस</option>
                                        @foreach($tsm_classes as $tsm_class)
                                            <option
                                                value="{{$tsm_class->id}}" {{($teacher->class->level == $tsm_class->level)?'selected':''}}>
                                                @foreach($levels as $key => $value)
                                                    @if($tsm_class->level == $key)
                                                        {{$value}}
                                                    @endif
                                                @endforeach
                                                -
                                                @foreach($classes as $key=>$value)
                                                    @if($tsm_class->class == $key)
                                                        {{$value}}
                                                    @endif
                                                @endforeach

                                            </option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('class'))
                                        <p class="text-danger">{{$errors->first('class')}}</p>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="formGroupExampleInput">फाईलहरु अपलोड गर्नुहोस:</label>
                                    <input type="file" class="form-control-file" id="formGroupExampleInput"
                                           placeholder="फाईल" name="files[]" multiple>
                                    @if($errors->has('files'))
                                        <p class="text-danger">{{$errors->first('files')}}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="mx-auto">
                                    <button class="btn btn-success" type="submit">बुझाउनुहोस</button>
                                </div>
                            </div>
                            <br>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div style="height: 50px" class="">
                                            <h6 class="text-center card-text" style="font-weight: bold">फाईलहरु</h6>
                                            <hr>
                                        </div>
                                        @foreach($teacher_files->where('file_type','slc_file') as $file)
                                            <div class=" d-inline-block teacherFile">
                                                <img src="{{asset('teachers_files/'.$file->file)}}" id="file" alt="img"
                                                     height="300px"
                                                     style="margin-right: 20px; margin-top: 20px">
                                                <button type="button" onclick="deleteImg({{$file->id}},this)"
                                                        class="btn btn-sm btn-danger d-block mt-2 mx-auto"
                                                >Delete
                                                </button>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div style="height: 50px" class="">
                                            <h6 class="text-center card-text" style="font-weight: bold">फाईलहरु</h6>
                                            <hr>
                                        </div>
                                        @foreach($teacher_files->where('file_type','higher_sec_file') as $file)
                                            <div class=" d-inline-block teacherFile">
                                                <img src="{{asset('teachers_files/'.$file->file)}}" id="file" alt="img"
                                                     height="300px"
                                                     style="margin-right: 20px; margin-top: 20px">
                                                <button type="button" onclick="deleteImg({{$file->id}},this)"
                                                        class="btn btn-sm btn-danger d-block mt-2 mx-auto"
                                                >Delete
                                                </button>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop
@section('script')
    <script type="text/javascript">
        function deleteImg(id, el) {
            file_id = id;

            var ajaxRoute = "{{route('delete-teacher-image')}}";
            $.ajax({
                type: 'POST',
                url: ajaxRoute,
                data: {
                    id: file_id
                },
                dataType: 'JSON',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                },
                success: function (msg) {
                    alert(msg.msg);
                    el.closest('.teacherFile').remove();
                }
            });
        }


    </script>
@stop


