@extends('layouts.teacher_salary_management')
@section('content')
    @php
        $levels = [
        'secondary'=>'माध्यमिक',
        'lower_secondary'=>'निम्न माध्यमिक',
        'primary_slc_pass'=>'प्राथमिक(एस.एल.सि उतीर्ण)',
        'primary_slc_pass_two'=>'प्राथमिक(दुई बिषयसम्म एस.एल.सि अनुतीर्ण)',
        'primary_slc_fail_more_two'=>'प्राथमिक(दुई बिषयभन्दा बढी एस.एल.सि अनुतीर्ण)'
        ];
    $classes = [
        'FC'=>'प्रथम श्रेणी',
        'SC'=>'द्धितीय श्रेणी',
        'TC'=>'तृतीय श्रेणी',

    ];
        $grades = [
            '1'=>'१',
            '2'=>'२',
            '3'=>'३',
            '4'=>'४',
            '5'=>'५',
            '6'=>'६',
            '7'=>'७',
            '8'=>'८',
            '9'=>'९',
            '10'=>'१०',
        ]

    @endphp
    <div class="container">
        <div class="mt-4">
            @include('messages.succFail')
        </div>
        <div class="row mt-4">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-center">शिक्षक थप्पनुहोस</div>
                    <div class="card-body">
                        <form method="post" action="{{route('add-teacher-action')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="first_name">नाम:</label>
                                    <input type="text" class="form-control" id="first_name"
                                           placeholder="नाम" name="first_name" value="{{old('first_name')}}">
                                    @if($errors->has('first_name'))
                                        <p class="text-danger">{{$errors->first('first_name')}}</p>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="middle_name">बिचको नाम:</label>
                                    <input type="text" class="form-control" id="middle_name"
                                           placeholder="बिचको नाम" name="middle_name" value="{{old('middle_name')}}">
                                    @if($errors->has('middle_name'))
                                        <p class="text-danger">{{$errors->first('middle_name')}}</p>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="last_name">थर:</label>
                                    <input type="text" class="form-control" id="last_name"
                                           placeholder="थर" name="last_name" value="{{old('last_name')}}">
                                    @if($errors->has('last_name'))
                                        <p class="text-danger">{{$errors->first('last_name')}}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="permanent_address">स्थायी ठेगाना:</label>
                                    <input type="text" class="form-control" id="permanent_address"
                                           placeholder="स्थायी ठेगाना" name="permanent_address"
                                           value="{{old('permanent_address')}}">
                                    @if($errors->has('permanent_address'))
                                        <p class="text-danger">{{$errors->first('permanent_address')}}</p>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="temporary_address">अस्थायी ठेगाना</label>
                                    <input type="text" class="form-control" id="temporary_address"
                                           placeholder="अस्थायी ठेगाना" name="temporary_address"
                                           value="{{old('temporary_address')}}">
                                    @if($errors->has('temporary_address'))
                                        <p class="text-danger">{{$errors->first('temporary_address')}}</p>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="date_of_birth">जन्म मिति:</label>
                                    <input type="date" class="form-control" id="date_of_birth"
                                           placeholder="अस्थायी ठेगाना" name="date_of_birth"
                                           value="{{old('date_of_birth')}}">
                                    @if($errors->has('date_of_birth'))
                                        <p class="text-danger">{{$errors->first('date_of_birth')}}</p>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="email">ईमेल:</label>
                                    <input type="email" class="form-control" id="email"
                                           placeholder="ईमेल" name="email" value="{{old('email')}}">
                                    @if($errors->has('email'))
                                        <p class="text-danger">{{$errors->first('email')}}</p>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="phone">फोन:</label>
                                    <input type="number" class="form-control" id="phone"
                                           placeholder="फोन" name="phone" value="{{old('phone')}}">
                                    @if($errors->has('phone'))
                                        <p class="text-danger">{{$errors->first('phone')}}</p>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="designation_date">नियुक्ति मिति:</label>
                                    <input type="date" class="form-control" id="start_date"
                                           placeholder="फोन" name="designation_date"
                                           value="{{old('designation_date')}}">
                                    @if($errors->has('designation_date'))
                                        <p class="text-danger">{{$errors->first('designation_date')}}</p>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="school">विद्यालय:</label>
                                    <select name="school" id="school" class="form-control">
                                        <option value="" selected disabled>विद्यालय छान्नुहोस</option>
                                        @foreach($schools as $school)
                                            <option value="{{$school->id}}">
                                                {{$school->school_name}}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('school'))
                                        <p class="text-danger">{{$errors->first('school')}}</p>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="subject">विषय:</label>
                                    <select name="subject" id="subject" class="form-control">
                                        <option value="" selected disabled>विषय छान्नुहोस</option>
                                        @foreach($subjects as $subject)
                                            <option value="{{$subject->id}}">
                                                {{$subject->subject_name}}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('subject'))
                                        <p class="text-danger">{{$errors->first('subject')}}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="grade">ग्रेड:</label>
                                    <select name="grade" id="grade" class="form-control">
                                        <option value="" selected disabled>ग्रेड छान्नुहोस</option>
                                        @foreach($tsm_grades as $tsm_grade)
                                            <option value="{{$tsm_grade->id}}">
                                                @foreach($grades as $key => $value)
                                                    @if($tsm_grade->grade_value == $key)
                                                        {{$value}}
                                                    @endif
                                                @endforeach
                                            </option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('grade'))
                                        <p class="text-danger">{{$errors->first('grade')}}</p>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="class">पद/श्रेणी छान्नुहोस:</label>
                                    <select name="class" id="class" class="form-control">
                                        <option value="" selected disabled>पद/श्रेणी छान्नुहोस</option>
                                        @foreach($tsm_classes as $tsm_class)
                                            <option value="{{$tsm_class->id}}">
                                                @foreach($levels as $key => $value)
                                                    @if($tsm_class->level == $key)
                                                        {{$value}}
                                                    @endif
                                                @endforeach
                                                -
                                                @foreach($classes as $key=>$value)
                                                    @if($tsm_class->class == $key)
                                                        {{$value}}
                                                    @endif
                                                @endforeach
                                            </option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('class'))
                                        <p class="text-danger">{{$errors->first('class')}}</p>
                                    @endif
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="slc">एस.एल.सि को कागजातहरु(Transcript,Mark sheet,Character
                                        Certificate):</label>
                                    <input type="file" class="form-control-file" id="slc"
                                           placeholder="एस.एल.सि को कागजातहरु" name="slc_files[]" multiple>
                                    @if($errors->has('slc_files'))
                                        <p class="text-danger">{{$errors->first('slc_files')}}</p>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="formGroupExampleInput">‌+2 को कागजातहरु(Transcript,Mark
                                        Sheet,Character,Migration,Provisional):</label>
                                    <input type="file" class="form-control-file" id="formGroupExampleInput"
                                           placeholder="‌+2 को कागजातहरु" name="higher_sec_files[]" multiple>
                                    @if($errors->has('higher_sec_files'))
                                        <p class="text-danger">{{$errors->first('higher_sec_files')}}</p>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="formGroupExampleInput">स्नातक को
                                        कागजादहरु(Transcript,Character):</label>
                                    <input type="file" class="form-control-file" id="formGroupExampleInput"
                                           placeholder="फाईल" name="bachelor_files[]" multiple>
                                    @if($errors->has('bachelor_files'))
                                        <p class="text-danger">{{$errors->first('bachelor_files')}}</p>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="formGroupExampleInput">मास्टर डिग्रिको कागजातहरु(Transcript):</label>
                                    <input type="file" class="form-control-file" id="formGroupExampleInput"
                                           placeholder="फाईल" name="masters_files[]" multiple>
                                    @if($errors->has('masters_files'))
                                        <p class="text-danger">{{$errors->first('masters_files')}}</p>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="formGroupExampleInput">नागरिकता(Front and Back):</label>
                                    <input type="file" class="form-control-file" id="formGroupExampleInput"
                                           placeholder="फाईल" name="citizenship_files[]" multiple>
                                    @if($errors->has('citizenship_files'))
                                        <p class="text-danger">{{$errors->first('citizenship_files')}}</p>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="formGroupExampleInput">फोटो:</label>
                                    <input type="file" class="form-control-file" id="formGroupExampleInput"
                                           placeholder="फाईल" name="image">
                                    @if($errors->has('image'))
                                        <p class="text-danger">{{$errors->first('image')}}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="text-center mb-2">
                                <button class="btn btn-success" type="submit">बुझाउनुहोस</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

