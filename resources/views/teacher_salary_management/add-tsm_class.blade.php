@extends('layouts.teacher_salary_management')
@section('content')

    @php
        $levels = [
        'secondary'=>'माध्यमिक',
        'lower_secondary'=>'निम्न माध्यमिक',
        'primary_slc_pass'=>'प्राथमिक(एस.एल.सि उतीर्ण)',
        'primary_slc_pass_two'=>'प्राथमिक(दुई बिषयसम्म एस.एल.सि अनुतीर्ण)',
        'primary_slc_fail_more_two'=>'प्राथमिक(दुई बिषयभन्दा बढी एस.एल.सि अनुतीर्ण)'
        ];
    $classes = [
        'FC'=>'प्रथम श्रेणी',
        'SC'=>'द्धितीय श्रेणी',
        'TC'=>'तृतीय श्रेणी',

    ]

    @endphp

    <div class="container">
        <div class="mt-4">
            @include('messages.succFail')
        </div>
        <div class="row mt-4">
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header text-center">पद/श्रेणी बिबरण थप्नुहोस</div>
                    <div class="card-body">
                        <form method="post" action="{{route('add-tsm_class-action')}}">
                            @csrf
                            <div class="form-group">
                                <label for="formGroupExampleInput">तह:</label>
                                <select name="level" id="select_class" class="form-control class">
                                    <option value="" selected disabled>तह छान्नुहोस</option>
                                    <option value="secondary">माध्यमिक</option>
                                    <option value="lower_secondary">निम्न माध्यमिक</option>
                                    <option value="primary_slc_pass">प्राथमिक(एस.एल.सि उतीर्ण)</option>
                                    <option value="primary_slc_fail_two" id="less_two">प्राथमिक(दुई बिषयसम्म एस.एल.सि
                                        अनुतीर्ण)
                                    </option>
                                    <option value="primary_slc_fail_more_two" id="more_two">प्राथमिक(दुई बिषयभन्दा बढी
                                        एस.एल.सि
                                        अनुतीर्ण)
                                    </option>
                                </select>
                                @if($errors->has('level'))
                                    <p class="text-danger">{{$errors->first('level')}}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">पद/श्रेणी:</label>
                                <select name="class" id="class" class="form-control">
                                    <option value="" selected disabled>तह छान्नुहोस</option>
                                    <option value="FC">प्रथम श्रेणी</option>
                                    <option value="SC">द्धितीय श्रेणी</option>
                                    <option value="TC">तृतीय श्रेणी</option>
                                </select>
                                @if($errors->has('class'))
                                    <p class="text-danger">{{$errors->first('class')}}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">तलब स्केल.:</label>
                                <input type="number" class="form-control" id="formGroupExampleInput"
                                       placeholder="तलब स्केल हाल्नुहोस" name="salary">
                                @if($errors->has('salary'))
                                    <p class="text-danger">{{$errors->first('salary')}}</p>
                                @endif
                            </div>
                            <div>
                                <button class="btn btn-success float-right" type="submit">बुझाउनुहोस</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header text-center">पद/श्रेणी बिबरण</div>
                    <div class="card-body">
                        <table class="table table-bordered text-center  table-sm">
                            <thead>
                            <th>क्रं.स.</th>
                            <th>तह</th>
                            <th>पद/श्रेणी</th>
                            <th>तलब/स्केल</th>
                            <th>कार्य</th>
                            </thead>
                            <tbody>
                            @foreach($tsm_classes as $tsm_class)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>
                                        @foreach($levels as $key => $value)
                                            @if($tsm_class->level == $key)
                                                {{$value}}
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($classes as $key => $value)
                                            @if($tsm_class->class == $key)
                                                {{$value}}
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>{{number_format($tsm_class->salary_scale,2,'.',',')}}</td>
                                    {{--                                    @if(Auth::User()->privilege != 'creator')--}}
                                    <td>
                                        <a href="{{route('edit-tsm_class',$tsm_class->id)}}"
                                           class="btn btn-sm btn-success">
                                            <li class="fa fa-edit"></li>
                                        </a>
                                        <a href="{{route('delete-tsm_class',$tsm_class->id)}}"
                                           class="btn btn-sm btn-danger">
                                            <li class="fa fa-trash"></li>
                                        </a>
                                    </td>
                                    {{--                                    @endif--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $("select.class").change(function () {
                if ($(this).val() == 'primary_slc_fail_two' || $(this).val() == 'primary_slc_fail_more_two') {
                    $('#class').attr('disabled', true);
                    // $("#class").append("<option value='none' selected='selected'></option>");
                } else {
                    $('#class').attr('disabled', false);
                    // $("#class").remove("<option value='none' selected='selected'></option>");

                }
            });
        });
    </script>
@endsection
