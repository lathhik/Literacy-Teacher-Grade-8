@extends('layouts.teacher_salary_management')
@section('content')

    @php
        $levels = [
        '1'=>'पहिलो',
        '2'=>'दोस्रो',
        '3'=>'तेस्रो',
        '4'=>'चोैंथो',
        '5'=>'पांचोै',
        '6'=>'छैंठोै',
        '7'=>'सातोैं',
        '8'=>'अाठोै',
        '9'=>'नवोैं',
        '10'=>'दशोैं',
        ];
    @endphp

    <div class="container">
        <div class="mt-4">
            @include('messages.succFail')
        </div>
        <div class="row mt-4">
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header text-center">पद थप्नुहोस</div>
                    <div class="card-body">
                        <form method="post" action="{{route('add-staff-position-action')}}">
                            @csrf
                            <div class="form-group">
                                <label for="school">पद:</label>
                                <input type="text" class="form-control" id="school" name="staff_position">
                                @if($errors->has('staff_position'))
                                    <p class="text-danger">{{$errors->first('staff_position')}}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="school_level">तह:</label>
                                <select name="position_level" id="school_level" class="form-control">
                                    <option value="" selected disabled>तह छान्नुहोस</option>
                                    <option value="1">{{'पहिलो'}}</option>
                                    <option value=2>{{'दोस्रो'}}</option>
                                    <option value="3">{{'तेस्रो'}}</option>
                                    <option value="4">{{'चोैंथो'}}</option>
                                    <option value="5">{{'पांचोै'}}</option>
                                    <option value="6">{{'छैंठोै'}}</option>
                                    <option value="7">{{'सातोैं'}}</option>
                                    <option value="8">{{'अाठोै'}}</option>
                                    <option value="9">{{'नवोैं'}}</option>
                                    <option value="10">{{'दशोैं'}}</option>
                                </select>
                                @if($errors->has('position_level'))
                                    <p class="text-danger">{{$errors->first('position_level')}}</p>
                                @endif
                            </div>
                            <div>
                                <button class="btn btn-success float-right" type="submit">बुझाउनुहोस</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header text-center">पद</div>
                    <div class="card-body">
                        <table class="table table-bordered text-center  table-sm">
                            <thead>
                            <th>क्रं.स.</th>
                            <th>पद</th>
                            <th>तह</th>
                            <th>कार्य</th>
                            </thead>
                            <tbody>
                            @foreach($staff_positions as $staff_position)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$staff_position->staff_position}}</td>
                                    <td>
                                    @foreach($levels as $key => $value)
                                        @if($staff_position->position_level == $key)
                                            {{$value}}
                                        @endif
                                    @endforeach
                                    <td>
                                        <a href="{{route('edit-staff-position',$staff_position->id)}}"
                                           class="btn btn-sm btn-success">
                                            <li class="fa fa-edit"></li>
                                        </a>
                                        <a href="{{route('delete-staff-position',$staff_position->id)}}"
                                           class="btn btn-sm btn-danger">
                                            <li class="fa fa-trash"></li>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>

            </div>
        </div>

    </div>
@stop
