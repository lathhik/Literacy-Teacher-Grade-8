@extends('layouts.teacher_salary_management')
@section('content')

    @php
        $levels = [
        '1'=>'पहिलो',
        '2'=>'दोस्रो',
        '3'=>'तेस्रो',
        '4'=>'चोैंथो',
        '5'=>'पांचोै',
        '6'=>'छैंठोै',
        '7'=>'सातोैं',
        '8'=>'अाठोै',
        '9'=>'नवोैं',
        '10'=>'दशोैं',
        ];

    @endphp

    <div class="container">
        <div class="mt-4">
            @include('messages.succFail')
        </div>
        <div class="row mt-4">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header text-center">पद थप्नुहोस</div>
                    <div class="card-body">
                        <form method="post" action="{{route('edit-staff-position-action',$staff_position->id)}}">
                            @csrf
                            <div class="form-group">
                                <label for="school">पद:</label>
                                <input type="text" class="form-control" id="school" name="staff_position"
                                       value="{{$staff_position->staff_position}}">
                                @if($errors->has('staff_position'))
                                    <p class="text-danger">{{$errors->first('staff_position')}}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="school_level">तह:</label>
                                <select name="position_level" id="school_level" class="form-control">
                                    <option value="" selected disabled>तह छान्नुहोस</option>
                                    <option
                                        value="1" {{$staff_position->position_level == 1?'selected':''}}>{{'पहिलो'}}</option>
                                    <option
                                        value=2" {{$staff_position->position_level == 2?'selected':''}}>{{'दोस्रो'}}</option>
                                    <option
                                        value="3" {{$staff_position->position_level == 3?'selected':''}}>{{'तेस्रो'}}</option>
                                    <option
                                        value="4" {{$staff_position->position_level == 4?'selected':''}}>{{'चोैंथो'}}</option>
                                    <option
                                        value="5" {{$staff_position->position_level == 5?'selected':''}}>{{'पांचोै'}}</option>
                                    <option
                                        value="6" {{$staff_position->position_level == 6?'selected':''}}>{{'छैंठोै'}}</option>
                                    <option
                                        value="7" {{$staff_position->position_level == 7?'selected':''}}>{{'सातोैं'}}</option>
                                    <option
                                        value="8" {{$staff_position->position_level == 8?'selected':''}}>{{'अाठोै'}}</option>
                                    <option
                                        value="9" {{$staff_position->position_level == 9?'selected':''}}>{{'नवोैं'}}</option>
                                    <option
                                        value="10" {{$staff_position->position_level == 10?'selected':''}}>{{'दशोैं'}}</option>
                                </select>
                                @if($errors->has('position_level'))
                                    <p class="text-danger">{{$errors->first('position_level')}}</p>
                                @endif
                            </div>
                            <div>
                                <button class="btn btn-success float-right" type="submit">बुझाउनुहोस</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>
@stop
