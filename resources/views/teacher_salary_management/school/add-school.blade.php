@extends('layouts.teacher_salary_management')
@section('content')

    @php
        $levels = [
        'secondary'=>'माध्यमिक',
        'lower_secondary'=>'निम्न माध्यमिक',
        'primary'=>'प्राथमिक',
        ];

    @endphp

    <div class="container">
        <div class="mt-4">
            @include('messages.succFail')
        </div>
        <div class="row mt-4">
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header text-center">विद्यालय थप्नुहोस</div>
                    <div class="card-body">
                        <form method="post" action="{{route('add-school-action')}}">
                            @csrf
                            <div class="form-group">
                                <label for="school">विद्यालयको नाम:</label>
                                <input type="text" class="form-control" id="school" name="school_name">
                                @if($errors->has('school_name'))
                                    <p class="text-danger">{{$errors->first('school_name')}}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="school_level">विद्यालयको तह:</label>
                                <select name="school_level" id="school_level" class="form-control">
                                    <option value="" selected disabled>विद्यालयको तह छान्नुहोस</option>
                                    <option value="secondary">{{'माध्यमिक'}}</option>
                                    <option value="lower_secondary">{{'निम्न माध्यमिक'}}</option>
                                    <option value="primary">{{'प्राथमिक'}}</option>
                                </select>
                                @if($errors->has('school_level'))
                                    <p class="text-danger">{{$errors->first('school_level')}}</p>
                                @endif
                            </div>
                            <div>
                                <button class="btn btn-success float-right" type="submit">बुझाउनुहोस</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header text-center">विद्यालयहरु</div>
                    <div class="card-body">
                        <table class="table table-bordered text-center  table-sm">
                            <thead>
                            <th>क्रं.स.</th>
                            <th>विद्यालयको नाम</th>
                            <th>विद्यालयको तह</th>
                            <th>कार्य</th>
                            </thead>
                            <tbody>
                            @foreach($schools as $school)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$school->school_name}}</td>
                                    <td>
                                    @foreach($levels as $key => $value)
                                        @if($school->school_type == $key)
                                            {{$value}}
                                        @endif
                                    @endforeach
                                    <td>
                                        <a href="{{route('edit-school',$school->id)}}"
                                           class="btn btn-sm btn-success">
                                            <li class="fa fa-edit"></li>
                                        </a>
                                        <a href="{{route('delete-school',$school->id)}}"
                                           class="btn btn-sm btn-danger">
                                            <li class="fa fa-trash"></li>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>

            </div>
        </div>

    </div>
@stop
