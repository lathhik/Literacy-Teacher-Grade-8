@extends('layouts.teacher_salary_management')
@section('content')
    <div class="container">
        <div class="mt-4">
            @include('messages.succFail')
        </div>
        <div class="row mt-4 ml-5">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header text-center">ग्रुप सम्पादन</div>
                    <div class="card-body">
                        <form method="post" action="{{route('edit-tsm_class-action',$tsm_class->id)}}">
                            @csrf
                            <div class="form-group">
                                <label for="formGroupExampleInput">तह:</label>
                                <select name="level" id="" class="form-control">
                                    <option value="" selected disabled>तह छान्नुहोस</option>
                                    <option value="secondary" {{($tsm_class->level =='secondary')?'selected':''}}>
                                        माध्यमिक
                                    </option>
                                    <option
                                        value="lower_secondary" {{($tsm_class->level =='lower_secondary')?'selected':''}}>
                                        निम्न माध्यमिक
                                    </option>
                                    <option
                                        value="primary_slc_pass" {{($tsm_class->level =='primary_slc_pass')?'selected':''}}>
                                        प्राथमिक(एस.एल.सि उतीर्ण)
                                    </option>
                                    <option
                                        value="primary_slc_fail_two" {{($tsm_class->level =='primary_slc_fail_two')?'selected':''}}>
                                        प्राथमिक(दुई बिषयसम्म एस.एल.सि अनुतीर्ण)
                                    </option>
                                    <option
                                        value="primary_slc_fail_more_two" {{($tsm_class->level =='primary_slc_fail_more_two')?'selected':''}}>
                                        प्राथमिक(दुई बिषयभन्दा बढी एस.एल.सि
                                        अनुतीर्ण)
                                    </option>
                                </select>
                                @if($errors->has('level'))
                                    <p class="text-danger">{{$errors->first('level')}}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">पद/श्रेणी:</label>
                                <select name="class" id="" class="form-control">
                                    <option value="" selected disabled>तह छान्नुहोस</option>
                                    <option value="FC" {{($tsm_class->class == 'FC')?'selected':''}}>प्रथम श्रेणी
                                    </option>
                                    <option value="SC" {{($tsm_class->class == 'SC')?'selected':''}}>द्धितीय श्रेणी
                                    </option>
                                    <option value="TC" {{($tsm_class->class == 'TC')?'selected':''}}>तृतीय श्रेणी
                                    </option>
                                </select>
                                @if($errors->has('class'))
                                    <p class="text-danger">{{$errors->first('class')}}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">तलब स्केल.:</label>
                                <input type="number" class="form-control" id="formGroupExampleInput"
                                       placeholder="तलब स्केल हाल्नुहोस" name="salary"
                                       value="{{$tsm_class->salary_scale}}">
                                @if($errors->has('salary'))
                                    <p class="text-danger">{{$errors->first('salary')}}</p>
                                @endif
                            </div>
                            <div>
                                <button class="btn btn-success float-right" type="submit">बुझाउनुहोस</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>
@stop
