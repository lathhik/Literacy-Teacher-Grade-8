@extends('layouts.teacher_salary_management')
@section('content')

    @php
        $levels = [
        'secondary'=>'माध्यमिक',
        'lower_secondary'=>'निम्न माध्यमिक',
        'primary'=>'प्राथमिक',
        ];

    @endphp

    <div class="container">
        <div class="mt-4">
            @include('messages.succFail')
        </div>
        <div class="row mt-4">
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header text-center">विषय थप्नुहोस</div>
                    <div class="card-body">
                        <form method="post" action="{{route('add-subject-action')}}">
                            @csrf
                            <div class="form-group">
                                <label for="subject">विषयको नाम:</label>
                                <input type="text" class="form-control" id="subject" name="subject_name">
                                @if($errors->has('subject_name'))
                                    <p class="text-danger">{{$errors->first('subject_name')}}</p>
                                @endif
                            </div>
                            <div>
                                <button class="btn btn-success float-right" type="submit">बुझाउनुहोस</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header text-center">विषयहरु</div>
                    <div class="card-body">
                        <table class="table table-bordered text-center  table-sm">
                            <thead>
                            <th>क्रं.स.</th>
                            <th>विषयको नाम</th>
                            <th>कार्य</th>
                            </thead>
                            <tbody>
                            @foreach($subjects as $subject)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$subject->subject_name}}</td>
                                    <td>
                                        <a href="{{route('edit-subject',$subject->id)}}"
                                           class="btn btn-sm btn-success">
                                            <li class="fa fa-edit"></li>
                                        </a>
                                        <a href="{{route('delete-subject',$subject->id)}}"
                                           class="btn btn-sm btn-danger">
                                            <li class="fa fa-trash"></li>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>

            </div>
        </div>

    </div>
@stop
