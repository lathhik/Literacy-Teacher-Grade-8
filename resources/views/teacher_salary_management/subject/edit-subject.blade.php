@extends('layouts.teacher_salary_management')
@section('content')

    @php
        $levels = [
        'secondary'=>'माध्यमिक',
        'lower_secondary'=>'निम्न माध्यमिक',
        'primary'=>'प्राथमिक',
        ];

    @endphp

    <div class="container">
        <div class="mt-4">
            @include('messages.succFail')
        </div>
        <div class="row mt-4 ml-4">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header text-center">विषय सम्पादन</div>
                    <div class="card-body">
                        <form method="post" action="{{route('edit-subject-action',$subject->id)}}">
                            @csrf
                            <div class="form-group">
                                <label for="subject">विषयको नाम:</label>
                                <input type="text" class="form-control" id="subject" name="subject_name"
                                       value="{{$subject->subject_name}}">
                                @if($errors->has('subject_name'))
                                    <p class="text-danger">{{$errors->first('subject_name')}}</p>
                                @endif
                            </div>
                            <div>
                                <button class="btn btn-success float-right" type="submit">बुझाउनुहोस</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>
@stop
