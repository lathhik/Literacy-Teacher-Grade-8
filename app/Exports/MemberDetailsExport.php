<?php

namespace App\Exports;

use App\Models\MemberDetails;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class MemberDetailsExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    use Exportable;
    /**
     * @var string
     */
    private $tole, $ward, $literacy,$ageFrom,$ageTo;

    public function __construct(string $ward, string $tole = null, string $literacy = null, string $ageFrom = null, string $ageTo = null)
    {
        $this->ward = $ward;
        $this->tole = $tole;
        $this->literacy = $literacy;
        $this->ageFrom = $ageFrom;
        $this->ageTo = $ageTo;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {

        $grade = [
            ''=>'',
            'basic' => 'आधारभुत',
            'higher_education' => 'उच्च शिक्षा',
            'secondary' => 'माध्यमिक',
            'informal' => 'अनोैपचारिक',

        ];
        $gender = [
            'male' => 'पुरुष',
            'female' => 'महिला',
            'other' => 'अन्य'
        ];

        $query = MemberDetails::query();
        if ($this->tole) {
            if ($this->tole == 'all') {
                $ward_id = $this->ward;
                $query = $query->whereHas('house', function ($q) use ($ward_id) {
                    $q->whereHas('tole', function ($q) use ($ward_id) {
                        $q->where('ward_id', $ward_id);
                    });
                });
            } else {
                $tole_id = $this->tole;
                $query = $query->whereHas('house', function ($q) use ($tole_id) {
                    $q->where('tole_id', $tole_id);
                });
            }
        }
        if ($this->literacy) {
            if ($this->literacy == 'literate') {
                $query->where('literacy', '1');
            } elseif ($this->literacy == 'illiterate') {
                $query->where('literacy', '0');
            }
        }
        if ($this->ageFrom!=null) {
            $query->where('age','>=', $this->ageFrom);
        }
        if ($this->ageTo!=null) {
            $query->where('age','<=', $this->ageTo);
        }
        $queries = $query->with('house.tole.ward', 'house.tole')->get();
        $members = collect([]);
        $count = 1;
        foreach ($queries as $query) {
            $member['s_no'] = $count;
            $member['full_name'] = $query->full_name;
            $member['gender'] = $gender[$query->gender];
            $member['age'] = $query->age;
            if (!$query->literacy) {
                $member['grade'] = 'निराक्षर';
            } else {
                $member['grade'] = $grade[$query->grade];
            }
            if ($query->school_status) {
                $member['school_status'] = 'छ';
            } else {
                $member['school_status'] = 'छेेैन';
            }
            $member['house'] = $query->house->house_no;
            $member['tole'] = $query->house->tole->tole;
            $member['ward'] = $query->house->tole->ward->ward_no;
            $members->push($member);
            $count++;
        }
        return $members;
    }

    public function headings(): array
    {
        return [
            '#',
            'पुरा नामथर',
            'लिङ्ग',
            'उमेर',
            'साक्षरता/तह',
            'विद्यालय गए/नगएको',
            'घर नं.',
            'टोल',
            'वडा'
        ];
    }
}
