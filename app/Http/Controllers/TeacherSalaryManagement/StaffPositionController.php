<?php

namespace App\Http\Controllers\TeacherSalaryManagement;

use App\TsmModels\StaffPosition;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StaffPositionController extends Controller
{
    public function addStaffPosition()
    {
        $staff_positions = StaffPosition::all();
        return view('teacher_salary_management.administrative_staff.staff_position.add-staff-position',
            compact('staff_positions'));
    }

    public function addStaffPositionAction(Request $request)
    {
        $this->validate($request, [
            'staff_position' => 'required',
            'position_level' => 'required'
        ]);

        $staff_position = new StaffPosition();
        $staff_position->staff_position = $request->staff_position;
        $staff_position->position_level = $request->position_level;

        if ($staff_position->save()) {
            return redirect()->back()->with('success', 'पद थपियो');
        }
    }

    public function editStaffPosition($id)
    {
        $staff_position = StaffPosition::find($id);
        return view('teacher_salary_management.administrative_staff.staff_position.edit-staff-position',
            compact('staff_position'));
    }

    public function editStaffPositionAction(Request $request, $id)
    {
        $this->validate($request, [
            'staff_position' => 'required',
            'position_level' => 'required',
        ]);

        $staff_position = StaffPosition::find($id);
        $staff_position->staff_position = $request->staff_position;
        $staff_position->position_level = $request->position_level;

        if ($staff_position->save()) {
            return redirect()->route('add-staff-position')->with('success', 'पद सम्पादन भयो');
        }
    }

    public function deleteStaffPosition($id)
    {
        $staff_position = StaffPosition::find($id);
        if ($staff_position->delete()) {
            return redirect()->back()->with('success', 'पद हटाइयो');
        }
        return redirect()->back()->with('fail', 'अान्तरिक त्रुटि');
    }
}
