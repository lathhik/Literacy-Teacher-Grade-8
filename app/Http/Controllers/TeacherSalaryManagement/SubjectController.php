<?php

namespace App\Http\Controllers\TeacherSalaryManagement;

use App\TsmModels\Subject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubjectController extends Controller
{
    public function addSubject()
    {
        $subjects = Subject::all();
        return view('teacher_salary_management.subject.add-subject', compact('subjects'));
    }

    public function addSubjectAction(Request $request)
    {
        $this->validate($request, [
            'subject_name' => 'required'
        ]);

        $subject = new Subject();
        $subject->subject_name = $request->subject_name;

        if ($subject->save()) {
            return redirect()->back()->with('success', 'विषय थपियो');
        }
    }

    public function editSubject($id)
    {
        $subject = Subject::find($id);
        return view('teacher_salary_management.subject.edit-subject', compact('subject'));
    }

    public function editSubjectAction(Request $request, $id)
    {
        $this->validate($request, [
            'subject_name' => 'required'
        ]);

        $subject = Subject::find($id);
        $subject->subject_name = $request->subject_name;

        if ($subject->save()) {
            return redirect()->route('add-subject')->with('success', 'विषय सम्पादन भयो');
        }
    }

    public function deleteSubject($id)
    {
        $subject = Subject::find($id);

        if ($subject->delete()) {
            return redirect()->back()->with('success', 'विषय हटाईयो');
        }
    }
}
