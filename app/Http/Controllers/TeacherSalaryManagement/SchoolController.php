<?php

namespace App\Http\Controllers\TeacherSalaryManagement;

use App\TsmModels\School;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SchoolController extends Controller
{
    public function addSchool()
    {
        $schools = School::all();
        return view('teacher_salary_management.school.add-school', compact('schools'));
    }

    public function addSchoolAction(Request $request)
    {
        $this->validate($request, [
            'school_name' => 'required',
            'school_level' => 'required'
        ]);

        $school = new School();
        $school->school_name = $request->school_name;
        $school->school_type = $request->school_level;

        if ($school->save()) {
            return redirect()->back()->with('success', 'विद्यालय थपियो');
        }
    }

    public function editSchool($id)
    {
        $school = School::find($id);
        return view('teacher_salary_management.school.edit-school', compact('school'));
    }

    public function editSchoolAction(Request $request, $id)
    {
        $this->validate($request, [
            'school_name' => 'required',
            'school_level' => 'required'
        ]);


        $school = School::find($id);
        $school->school_name = $request->school_name;
        $school->school_type = $request->school_level;

        if ($school->save()) {
            return redirect()->route('add-school')->with('success', 'विद्यालय सम्पादन भयो');
        }
    }

    public function deleteSchool($id)
    {
        $school = School::find($id);

        if ($school->delete()) {
            return redirect()->back()->with('success', 'विद्यालय मेतियो');
        }
    }
}
