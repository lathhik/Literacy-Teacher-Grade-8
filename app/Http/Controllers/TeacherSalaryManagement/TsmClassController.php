<?php

namespace App\Http\Controllers\TeacherSalaryManagement;

use App\TsmModels\TsmClass;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TsmClassController extends Controller
{
    public function addTsmClass(Request $request)
    {
        $tsm_classes = TsmClass::all();
        return view('teacher_salary_management.add-tsm_class', compact('tsm_classes'));
    }

    public function addTsmClassAction(Request $request)
    {
        $this->validate($request, [
            'salary' => 'required',
//            'class' => 'required',
            'level' => 'required',
        ]);

        $tsm_class = new TsmClass();
        $tsm_class->salary_scale = $request->salary;
        $tsm_class->class = $request->class;
        $tsm_class->level = $request->level;

        if ($tsm_class->save()) {
            return redirect()->back()->with('success', 'Class details added');
        }
    }

    public function editTsmClass($id)
    {
        $tsm_class = TsmClass::find($id);
        return view('teacher_salary_management.edit-tsm_class', compact('tsm_class'));
    }

    public function editTsmClassAction(Request $request, $id)
    {
        $tsm_class = TsmClass::find($id);

        $this->validate($request, [
            'salary' => 'required',
//            'class' => 'required',
            'level' => 'required'
        ]);

        $tsm_class->salary_scale = $request->salary;
        $tsm_class->level = $request->level;
        $tsm_class->class = $request->class;

        if ($tsm_class->save()) {
            return redirect()->route('add-tsm_class')->with('success', 'Class Successfully edited');
        }
    }

    public function deleteTsmClass($id)
    {
        $tsm_class = TsmClass::find($id);

        if ($tsm_class->delete()) {
            return redirect()->back()->with('success', 'Class was successfully deleted');
        }
    }
}
