<?php

namespace App\Http\Controllers\TeacherSalaryManagement;

use App\TsmModels\Teacher;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        $teachers = Teacher::all();

        $teachers_secondary = Teacher::whereHas('class', function ($q) {
            $q->where('level', 'secondary');
        })->count();
        $teachers_lower_secondary = Teacher::whereHas('class', function ($q) {
            $q->where('level', 'lower_secondary');
        })->count();
        $teachers_primary = Teacher::whereHas('class', function ($q) {
            $q->where('level', 'primary');
        })->count();
        return view('teacher_salary_management.home', compact('teachers', 'teachers_secondary', 'teachers_lower_secondary'));
    }
}
