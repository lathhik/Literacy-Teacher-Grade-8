<?php

namespace App\Http\Controllers\TeacherSalaryManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class TsmAdminAuthController extends Controller
{
    use AuthenticatesUsers;


    public function tsmLogin()
    {
        return view('teacher_salary_management.auth.tsm-login');
    }

    public function tsmAdminLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $credentials = $request->only('email', 'password');

//        return $credentials;

        $remember_token = false;

        if ($request->input('remember')) {
            $remember_token = true;
        }

        if (Auth::guard('tsmAdmin')->attempt($credentials, $remember_token)) {
            return redirect()->intended(route('tsm-index'));
        }
        return redirect()->back()->with('fail', 'Invalid Credentials');
    }

    public function tsmLogout()
    {
        Auth::guard('tsmAdmin')->logout();
        return redirect()->route('tsm-login');
    }
}
