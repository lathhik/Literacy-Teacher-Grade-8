<?php

namespace App\Http\Controllers\TeacherSalaryManagement;

use App\TsmModels\TsmGrade;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TsmGradeController extends Controller
{
    public function addTsmGrade()
    {
        $tsm_grades = TsmGrade::all();
        return view('teacher_salary_management.add-tsm_grade', compact('tsm_grades'));
    }

    public function addTsmGradeAction(Request $request)
    {
        $this->validate($request, [
            'grade' => 'required'
        ]);

        $tsm_grade = new TsmGrade();
        $tsm_grade->grade_value = $request->grade;
        $tsm_grade->grade_name = $request->grade;

        if ($tsm_grade->save()) {
            return redirect()->back()->with('success', 'Grade was successfully added');
        }
    }

    public function editTsmGrade($id)
    {
        $tsm_grade = TsmGrade::find($id);
        return view('teacher_salary_management.edit-tsm_grade', compact('tsm_grade'));
    }

    public function editTsmGradeAction(Request $request, $id)
    {
        $tsm_grade = TsmGrade::find($id);

        $this->validate($request, [
            'grade' => 'required'
        ]);

        $tsm_grade->grade_value = $request->grade;
        $tsm_grade->grade_name = $request->grade;

        if ($tsm_grade->save()) {
            return redirect()->route('add-tsm_grade')->with('success', 'Grade was successfully edited');
        }
    }

    public function deleteTsmGrade($id)
    {
        $tsm_grade = TsmGrade::find($id);

        if ($tsm_grade->delete()) {
            return redirect()->back()->with('success', 'Grade Was successfully deleted');
        }
    }
}
