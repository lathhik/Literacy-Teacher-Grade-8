<?php

namespace App\Http\Controllers\TeacherSalaryManagement;

use App\TsmModels\School;
use App\TsmModels\Subject;
use App\TsmModels\Teacher;
use App\TsmModels\TsmClass;
use App\TsmModels\TsmFile;
use App\TsmModels\TsmGrade;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;

class TeacherController extends Controller
{

    public function addTeacher()
    {
        $teachers = Teacher::all();
        $tsm_classes = TsmClass::all();
        $tsm_grades = TsmGrade::all();
        $schools = School::all();
        $subjects = Subject::all();
        return view('teacher_salary_management.add-teacher', compact('teachers',
            'tsm_classes',
            'tsm_grades',
            'schools',
            'subjects'));
    }

    public function viewTeacher()
    {
        $teachers = Teacher::all();
        return view('teacher_salary_management.view-teacher', compact('teachers'));
    }

    public function addTeacherAction(Request $request)
    {

//        return $request->all();

        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'permanent_address' => 'required',
            'temporary_address' => 'required',
            'date_of_birth' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'designation_date' => 'required',
            'school' => 'required',
            'subject' => 'required',
            'grade' => 'required',
            'class' => 'required',
            'citizenship_files' => 'required',
            'image' => 'required',
        ]);

        $teacher = new Teacher();
        $teacher->class_id = $request->class;
        $teacher->grade_id = $request->grade;
        $teacher->subject_id = $request->subject;
        $teacher->school_id = $request->school;
        $teacher->first_name = $request->first_name;
        $teacher->middle_name = $request->middle_name;
        $teacher->last_name = $request->last_name;
        $teacher->permanent_address = $request->permanent_address;
        $teacher->temporary_address = $request->temporary_address;
        $teacher->dob = $request->date_of_birth;
        $teacher->designation_date = $request->designation_date;
        $teacher->email = $request->email;
        $teacher->phone = $request->phone;

        $teacher->save();

        $files_slc = $request->file('slc_files');
        if ($files_slc) {
            if (count($files_slc) >= 1) {
                foreach ($files_slc as $file_slc) {
                    $new_name_slc = str_random(20) . '.' . $file_slc->getClientOriginalExtension();
                    $file_img = Image::make($file_slc);

                    if (!file_exists(public_path('teachers_files'))) {
                        mkdir(public_path('teachers_files'));
                    }

                    $file_img->save(public_path('teachers_files/' . $new_name_slc));
                    $teacher_file = new TsmFile();
                    $teacher_file->file = $new_name_slc;
                    $teacher_file->file_type = 'slc_file';
                    $teacher_file->teacher_id = $teacher->id;
                    $teacher_file->save();
                }
            }

        }

        $files_higher_sec = $request->file('higher_sec_files');
        if ($files_higher_sec) {
            if (count($files_higher_sec) >= 1) {
                foreach ($files_higher_sec as $file_higher_sec) {
                    $new_name_higher_sec = str_random(20) . '.' . $file_higher_sec->getClientOriginalExtension();
                    $file_img = Image::make($file_higher_sec);

                    if (!file_exists(public_path('teachers_files'))) {
                        mkdir(public_path('teachers_files'));
                    }

                    $file_img->save(public_path('teachers_files/' . $new_name_higher_sec));

                    $teacher_file = new TsmFile();
                    $teacher_file->file = $new_name_higher_sec;
                    $teacher_file->file_type = 'higher_sec_file';
                    $teacher_file->teacher_id = $teacher->id;
                    $teacher_file->save();
                }

            }

        }

        $files_bachelor = $request->file('bachelor_files');
        if ($files_bachelor) {
            if (count($files_bachelor) >= 1) {
                foreach ($files_bachelor as $file_bachelor) {
                    $new_name_bachelor = str_random(20) . '.' . $file_bachelor->getClientOriginalExtension();
                    $file_img = Image::make($file_bachelor);

                    if (!file_exists(public_path('teachers_files'))) {
                        mkdir(public_path('teachers_files'));
                    }

                    $file_img->save(public_path('teachers_files/' . $new_name_bachelor));
                    $teacher_file = new TsmFile();
                    $teacher_file->file = $new_name_bachelor;
                    $teacher_file->file_type = 'bachelor_file';
                    $teacher_file->teacher_id = $teacher->id;
                    $teacher_file->save();
                }

            }

        }

        $files_masters = $request->file('masters_files');
        if ($files_masters) {
            if (count($files_masters) >= 1) {
                foreach ($files_masters as $file_master) {
                    $new_name_master = str_random(20) . '.' . $file_master->getClientOriginalExtension();
                    $file_img = Image::make($file_master);

                    if (!file_exists(public_path('teachers_files'))) {
                        mkdir(public_path('teachers_files'));
                    }

                    $file_img->save(public_path('teachers_files/' . $new_name_master));
                    $teacher_file = new TsmFile();
                    $teacher_file->file = $new_name_master;
                    $teacher_file->file_type = 'master_file';
                    $teacher_file->teacher_id = $teacher->id;
                    $teacher_file->save();
                }

            }

        }

        $files_citizenship = $request->file('citizenship_files');
        if ($files_citizenship) {
            if (count($files_citizenship) >= 1) {
                foreach ($files_citizenship as $file_citizenship) {
                    $new_name_citizenship = str_random(20) . '.' . $file_citizenship->getClientOriginalExtension();
                    $file_img = Image::make($file_citizenship);

                    if (!file_exists(public_path('teachers_files'))) {
                        mkdir(public_path('teachers_files'));
                    }

                    $file_img->save(public_path('teachers_files/' . $new_name_citizenship));
                    $teacher_file = new TsmFile();
                    $teacher_file->file = $new_name_citizenship;
                    $teacher_file->file_type = 'citizenship_file';
                    $teacher_file->teacher_id = $teacher->id;
                    $teacher_file->save();
                }

            }

        }

        $image = $request->file('image');
        if ($image) {
            $new_name_image = str_random(20) . '.' . $image->getClientOriginalExtension();
            $file_img = Image::make($image);

            if (!file_exists(public_path('teachers_files'))) {
                mkdir(public_path('teachers_files'));
            }

            $file_img->save(public_path('teachers_files/' . $new_name_image));
            $teacher_file = new TsmFile();
            $teacher_file->file = $new_name_image;
            $teacher_file->file_type = 'image';
            $teacher_file->teacher_id = $teacher->id;
            $teacher_file->save();
        }
        return redirect()->back()->with('success', 'शिक्षक थपियो');
    }

    public function editTeacher($id)
    {
        $teacher = Teacher::find($id);
        $tsm_classes = TsmClass::all();
        $tsm_grades = TsmGrade::all();
        $schools = School::all();
        $subjects = Subject::all();
        $teacher_files = TsmFile::where('teacher_id', $teacher->id)->where('file_type', 'slc_file')->get();
        return view('teacher_salary_management.edit-teacher', compact('teacher',
            'tsm_classes',
            'tsm_grades',
            'teacher_files',
            'subjects',
            'schools'));
    }

    public function editTeacherAction(Request $request, $id)
    {
        return $request->all();
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'permanent_address' => 'required',
            'temporary_address' => 'required',
            'date_of_birth' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'designation_date' => 'required',
            'school' => 'required',
            'subject' => 'required',
            'grade' => 'required',
            'class' => 'required',
        ]);

        $teacher = Teacher::find($id);
        $teacher->class_id = $request->class;
        $teacher->grade_id = $request->grade;
        $teacher->school_id = $request->school_id;
        $teacher->subject_id = $request->subject_id;
        $teacher->first_name = $request->first_name;
        $teacher->middle_name = $request->middle_name;
        $teacher->last_name = $request->last_name;
        $teacher->permanent_address = $request->permanent_address;
        $teacher->temporary_address = $request->temporary_address;
        $teacher->dob = $request->date_of_birth;
        $teacher->phone = $request->phone;
        $teacher->email = $request->email;

        $teacher->save();

        if ($request->hasFile('files')) {
            $files = $request->file('files');
            foreach ($files as $file) {
                $new_name = str_random(20) . '.' . $file->getClientOriginalExtension();
                $img_file = Image::make($file);

                if (!file_exists(public_path('teachers_files'))) {
                    mkdir(public_path('teachers_files'));
                }

                $img_file->save(public_path('teachers_files/' . $new_name));

                $teacher_file = new TsmFile();

                $teacher_file->teacher_id = $teacher->id;
                $teacher_file->file = $new_name;

                $teacher_file->save();
            }
        }

        return redirect()->route('view-teacher')->with('success', 'शिक्षक सम्मपादन भयो');
    }

    public function ViewTeacherDetails($id)
    {
        $teacher = Teacher::findOrFail($id);
        $teacher_files = TsmFile::where('teacher_id', $teacher->id)->get();
        $image = TsmFile::where('teacher_id', $teacher->id)->where('file_type', 'image')->first();

        return view('teacher_salary_management.teacher.view-teacher-details', compact('teacher',
            'teacher_files',
            'image'));
    }

    public function deleteTeacherImage(Request $request)
    {
        $teacher_file = TsmFile::find($request->id);

        if (file_exists(public_path('teachers_files/' . $teacher_file->file))) {
            unlink(public_path('teachers_files/' . $teacher_file->file));
        }

        $teacher_file->delete();
        return response()->json(['msg' => 'Image Deleted']);
    }

    public function deleteTeacher($id)
    {
        $teacher = Teacher::find($id);
        $teacher_files = TsmFile::where('teacher_id', $teacher->id)->get();

        if (count($teacher_files) > 1) {
            foreach ($teacher_files as $file) {
                if (file_exists(public_path('teachers_file/' . $file->file))) {
                    unlink(public_path('teachers_files/' . $file->file));
                }
                $file->delete();
            }
            if ($teacher->delete()) {
                return redirect()->back()->with('success', 'शिक्षक हटाईयो');
            }
        }
    }

    public function downloadFile($id)
    {
        $file = TsmFile::find($id);
        $file_to_down = public_path('teachers_files/' . $file->file);
        return response()->download($file_to_down);
    }
}
