<?php

namespace App\Http\Controllers\TeacherSalaryManagement;

use App\TsmModels\AdminStaff;
use App\TsmModels\StaffPosition;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminStaffController extends Controller
{
    public function addStaff()
    {
        $staffs = AdminStaff::all();
        return view('teacher_salary_management.administrative_staff.staff.add-staff',
            compact('staffs'));
    }
}
