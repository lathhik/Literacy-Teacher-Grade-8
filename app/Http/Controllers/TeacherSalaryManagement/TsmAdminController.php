<?php

namespace App\Http\Controllers\TeacherSalaryManagement;

use App\TsmModels\TsmAdmin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class TsmAdminController extends Controller
{
    public function addTsmAdmin()
    {
        $tsm_admins = TsmAdmin::all();
        return view('teacher_salary_management.admin.add-tsm_admin', compact('tsm_admins'));
    }

    public function addTsmAdminAction(Request $request)
    {
//        return $request->all();
        $this->validate($request, [
            'full_name' => 'required',
            'email' => 'required',
            'password' => 'required|confirmed',
            'privilege' => 'required'

        ]);

        $tsm_admin = new TsmAdmin();
        $tsm_admin->full_name = $request->full_name;
        $tsm_admin->email = $request->email;
        $tsm_admin->password = $request->password;
        $tsm_admin->privilege = $request->privilege;

        if ($tsm_admin->save()) {
            return redirect()->back()->with('success', 'Admin was successfully added');
        }

    }

    public function editTsmAdmin($id)
    {
        $tsm_admin = TsmAdmin::find($id);
        return view('teacher_salary_management.admin.edit-tsm_admin', compact('tsm_admin'));
    }

    public function editTsmAdminAction(Request $request, $id)
    {
        $tsm_admin = TsmAdmin::find($id);
        $this->validate($request, [
            'full_name' => 'required',
            'email' => [
                'required',
                'email',
                Rule::unique('tsm_admins')->ignore($tsm_admin->id)
            ],
            'privilege' => 'required',
            'password' => [
                'nullable',
                'min:6'
            ]
        ]);

        $tsm_admin->full_name = $request->full_name;
        $tsm_admin->email = $request->email;
        $tsm_admin->privilege = $request->privilege;

        if ($request->password) {
            $tsm_admin->password = bcrypt($request->password);
        }

        if ($tsm_admin->save()) {
            return redirect()->route('add-tsm_admin')->with('success', 'Admin was successfully edited');
        }


    }

    public function deleteTsmAdmin($id)
    {
        $tsm_admin = TsmAdmin::find($id);

        if ($tsm_admin->delete()) {
            return redirect()->back()->with('success', 'Admin Was successfully deleted');
        }
    }
}
