<?php

namespace App\Http\Controllers\LiteracyAnnouncement;

use App\Http\Controllers\Controller;
use App\Models\Tole;
use App\Models\Ward;
use Illuminate\Http\Request;

class ToleController extends Controller
{
    public function tole()
    {
        $wards = Ward::all();
        $toles = Tole::all();
        return view('literacy_announcement.add-tole')
            ->with('toles', $toles)
            ->with('wards', $wards);
    }

    public function addTole(Request $request)
    {
        $this->validate($request, [
            'tole' => 'required|unique:toles,tole',
            'ward_id' => 'required'
        ]);

        $tole = new Tole();
        $tole->tole = $request->tole;
        $tole->ward_id = $request->ward_id;

        if ($tole->save()) {
            return redirect()->back()->with('success', 'Tole was Successfully added');
        }
    }

    public function editTole($id)
    {
        $wards = Ward::all();
        $toles = Tole::all();
        $selectedTole = Tole::find($id);
        return view('literacy_announcement.edit-tole')
            ->with('toles', $toles)
            ->with('selectedTole', $selectedTole)
            ->with('wards', $wards);
    }

    public function updateTole(Request $request, $id)
    {
        $this->validate($request, [
            'tole' => 'required|unique:toles,tole',
            'ward_id' => 'required'
        ]);

        $tole = Tole::find($id);
        $tole->tole = $request->tole;
        $tole->ward_id = $request->ward_id;
        $tole->save();
        return redirect()->route('tole');
    }
}
