<?php

namespace App\Http\Controllers\LiteracyAnnouncement;

use App\Http\Controllers\Controller;
use App\Models\House;
use Illuminate\Http\Request;

class HouseController extends Controller
{
    public function house()
    {
        return view('literacy_announcement.add-house');
    }

    public function viewHouseDetails()
    {
        $house = House::with('members', 'tole')->get();
        return view('literacy_announcement.view-house-details')->with('houses', $house);
    }
    public function showHouse($id)
    {
        $house = House::with('members')->find($id);
        return view('literacy_announcement.show-house',compact('house'));
    }

}
