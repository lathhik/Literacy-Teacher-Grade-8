<?php

namespace App\Http\Controllers\LiteracyAnnouncement;

use App\Exports\MemberDetailsExport;
use App\Http\Controllers\Controller;
use App\Models\House;
use App\Models\MemberDetails;
use App\Models\Tole;
use App\Models\Ward;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MemberDetailsController extends Controller
{
    public function memberDetail()
    {
        $wards = Ward::all();
        $toles = Tole::all();
        return view('literacy_announcement.add-member-details', compact('wards', 'toles'));
    }

    public function getToleWard(Request $request)
    {
        $id = $request->id;
        $toles = Tole::where('ward_id', $id)->get();
        return $toles;
    }

    public function addMemberDetails(Request $request)
    {
        DB::beginTransaction();
        try {
            $house = new House();
            $house->tole_id = $request->tole;
            $house->house_no = $request->house_no;
            $house->save();
            $first_name = $request->first_name;
            $middle_name = $request->middle_name;
            $last_name = $request->last_name;
            $age = $request->age;
            $rel_to_head = $request->rel_to_head;
            $gender = $request->gender;
            $literacy = $request->literacy;
            $grade = $request->grade;
            $school_status = $request->school_status;
            $remarks = $request->remarks;
            for ($i = 0; $i < (count($first_name)); $i++) {
                $member_details = new MemberDetails();
                $member_details->house_id = $house->id;
                $member_details->first_name = $first_name[$i];
                $member_details->middle_name = $middle_name[$i];
                $member_details->last_name = $last_name[$i];
                $member_details->age = $age[$i];
                $member_details->relation_to_head = $rel_to_head[$i];
                $member_details->gender = $gender[$i];
                $member_details->literacy = $literacy[$i];
                if ($literacy[$i] == 1) {
                    $member_details->grade = $grade[$i] ?? 'basic';
                    $member_details->school_status = $school_status[$i] ?? '0';
                }
                $member_details->remarks = $remarks[$i];
                $member_details->save();
            }
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('fail', "विवरण थप भएन");
        }
        DB::commit();
        return redirect()->back()->with('success', "विवरण थप भएको छ");
    }

    public function editHouseDetails($id)
    {
        $wards = Ward::all();
        $house = House::with('members')->find($id);

        return view('literacy_announcement.edit-member-details')
            ->with('house', $house)
            ->with('wards', $wards);
    }

    public function editMemberDetailAction(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $house = House::find($id);
            $house->tole_id = $request->tole;
            $house->house_no = $request->house_no;
            $house->save();

            $ids = $request->id;
            $diff = $house->members->diff($house->members()->whereIn('id', $ids)->get());
            $diff->each->delete();

            $first_name = $request->first_name;
            $middle_name = $request->middle_name;
            $last_name = $request->last_name;
            $age = $request->age;
            $rel_to_head = $request->rel_to_head;
            $gender = $request->gender;
            $literacy = $request->literacy;
            $grade = $request->grade;
            $remarks = $request->remarks;
            $k = 0;
            for ($i = 0; $i < (count($ids)); $i++) {
                $member_detail = MemberDetails::find($ids[$i]);
                $member_detail->house_id = $house->id;
                $member_detail->first_name = $first_name[$i];
                $member_detail->middle_name = $middle_name[$i];
                $member_detail->last_name = $last_name[$i];
                $member_detail->age = $age[$i];
                $member_detail->relation_to_head = $rel_to_head[$i];
                $member_detail->gender = $gender[$i];
                $member_detail->literacy = $literacy[$i];
                if ($literacy[$i] == 1) {
                    $member_detail->grade = $grade[$i] ?? 'basic';
                    $member_detail->school_status = $school_status[$i] ?? '0';
                }
                $member_detail->remarks = $remarks[$i];
                $member_detail->save();
            }
            for ($i = $i; $i < (count($first_name)); $i++) {
                $member_detail = new MemberDetails();
                $member_detail->house_id = $house->id;
                $member_detail->first_name = $first_name[$i];
                $member_detail->middle_name = $middle_name[$i];
                $member_detail->last_name = $last_name[$i];
                $member_detail->age = $age[$i];
                $member_detail->relation_to_head = $rel_to_head[$i];
                $member_detail->gender = $gender[$i];
                $member_detail->literacy = $literacy[$i];
                if ($literacy[$i] == 1) {
                    $member_detail->grade = $grade[$i] ?? 'basic';
                    $member_detail->school_status = $school_status[$i] ?? '0';
                }
                $member_detail->remarks = $remarks[$i];
                $member_detail->save();
            }
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back();
        }
        DB::commit();
        return redirect()->route('view-house-details');
    }

    public function viewAllMembers(Request $request)
    {
        $wards = Ward::all();
        $selectedVars = [
            'ward' => $request->has('ward') ? $request->ward : 'all',
            'tole' => $request->has('tole') ? $request->tole : 'all',
            'literacy' => $request->has('literacy') ? $request->literacy : 'both',
            'ageFrom' => $request->ageFrom,
            'ageTo' => $request->ageTo,
        ];
        $query = MemberDetails::query();
        if ($request->has('tole')) {
            if ($request->tole == 'all') {
                $ward_id = $request->ward;
                $query = $query->whereHas('house', function ($q) use ($ward_id) {
                    $q->whereHas('tole', function ($q) use ($ward_id) {
                        $q->where('ward_id', $ward_id);
                    });
                });
            } else {
                $tole_id = $request->tole;
                $query = $query->whereHas('house', function ($q) use ($tole_id) {
                    $q->where('tole_id', $tole_id);
                });
            }
        }
        if ($request->has('literacy')) {
            if ($request->literacy == 'literate') {
                $query->where('literacy', '1');
            } elseif ($request->literacy == 'illiterate') {
                $query->where('literacy', '0');
            }
        }
        if ($request->has('ageFrom') && $request->input('ageFrom')!=null) {
            $query->where('age','>=', $request->ageFrom);
        }
        if ($request->has('ageTo')  && $request->input('ageTo')!=null) {
            $query->where('age','<=', $request->ageTo);
        }
        $members = $query->paginate(50);
        return view('literacy_announcement.view-all-members', compact('members', 'wards', 'selectedVars'));
    }

    public function exportMemberDetails(Request $request)
    {
        $ward = $request->ward;
        $tole = $request->tole;
        $literacy = $request->literacy;
        $ageFrom = $request->ageFrom;
        $ageTo = $request->ageTo;

        return (new MemberDetailsExport($ward, $tole, $literacy,$ageFrom,$ageTo))->download('memberDetails.xlsx');
    }
}
