<?php

namespace App\Http\Controllers\LiteracyAnnouncement\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;


    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('literacy_announcement.auth.login');
    }
    /**
     * Where to redirect users after login.
     *
     * @var string
     */


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function redirectTo()
    {
        if (Auth::user()->privilege == 'super_admin') {
            return route('home');
        } else {
            return route('ward');
        }
    }

    protected function loggedOut(Request $request)
    {
        return redirect()->route('login');
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();
//        $request->session()->invalidate();
        return $this->loggedOut($request) ?: redirect('/');
    }
}
