<?php

namespace App\Http\Controllers\LiteracyAnnouncement;

use App\Http\Controllers\Controller;
use App\Models\House;
use App\Models\MemberDetails;
use App\Models\Tole;
use App\Models\Ward;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return \Illuminate\Http\RedirectResponse
     */

    public function getLogin()
    {
        return redirect()->route('login');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $member_count = MemberDetails::count();
        $male_count = MemberDetails::where('gender', 'male')->count();
        $female_count = MemberDetails::where('gender', 'female')->count();
        $ward_count = Ward::count();
        $tole_count = Tole::count();
        $house_count = House::count();
        return view('literacy_announcement.home', compact('member_count', 'ward_count', 'tole_count', 'house_count', 'male_count', 'female_count'));
    }

    public function viewReports()
    {
        $wards = Ward::all();
        $filter = 'gender';
        $query = MemberDetails::query();
        $query1 = $query->get();
        $filteredData = collect([
            'literate_male' => $this->filterByGender($query1, 'male', '1'),
            'literate_female' => $this->filterByGender($query1, 'female', '1'),
            'illiterate_male' => $this->filterByGender($query1, 'male', '0'),
            'illiterate_female' => $this->filterByGender($query1, 'female', '0'),
        ]);
        $selectedVars = ['ward' => 'all', 'tole' => ''];
        return view('literacy_announcement.Reports.reports', compact('wards', 'filter', 'filteredData', 'selectedVars'));
    }

    public function filterReports(Request $request)
    {
        $selectedVars = ['ward' => 'all', 'tole' => ''];
        $wards = Ward::all();
        $filter = $request->filter_by;

        $query = MemberDetails::query();

        $total_population = count(MemberDetails::all());
        if ($request->has('tole')) {
            $selectedVars['ward'] = $request->ward;
            if ($request->tole == 'all') {
                $ward_id = $request->ward;
                $query = $query->whereHas('house', function ($q) use ($ward_id) {
                    $q->whereHas('tole', function ($q) use ($ward_id) {
                        $q->where('ward_id', $ward_id);
                    });
                });
            } else {
                $selectedVars['tole'] = $request->tole;
                $tole_id = $request->tole;
                $query = $query->whereHas('house', function ($q) use ($tole_id) {
                    $q->where('tole_id', $tole_id);
                });
            }
        }

        $query1 = $query->get();
        if ($filter == 'gender') {
            $filteredData = collect([
                'literate_male' => $this->filterByGender($query1, 'male', '1'),
                'literate_female' => $this->filterByGender($query1, 'female', '1'),
                'illiterate_male' => $this->filterByGender($query1, 'male', '0'),
                'illiterate_female' => $this->filterByGender($query1, 'female', '0'),
            ]);
        } elseif ($filter == 'age_group') {
            $ageFrom = $request->input('ageFrom');
            $ageTo = $request->input('ageTo');
            $filteredData = collect([
                'ageFrom'=>$ageFrom,
                'ageTo'=>$ageTo,
                'literate_male' => $this->filterByAgeGroup($query1, 'male', $ageFrom, $ageTo, 1),
                'literate_female' => $this->filterByAgeGroup($query1, 'female', $ageFrom, $ageTo, 1),
                'illiterate_male' => $this->filterByAgeGroup($query1, 'male', $ageFrom, $ageTo, 0),
                'illiterate_female' => $this->filterByAgeGroup($query1, 'female', $ageFrom, $ageTo, 0),
            ]);
        } elseif ($filter == 'school_status') {
            $filteredData = collect([
                'school_status_male_yes' => $this->filterBySchoolStatus($query1, 'male', 0, 5, 1),
                'school_status_female_yes' => $this->filterBySchoolStatus($query1, 'female', 0, 5, 1),
                'school_status_male_no' => $this->filterBySchoolStatus($query1, 'male', 0, 5, 0),
                'school_status_female_no' => $this->filterBySchoolStatus($query1, 'female', 0, 5, 0),
                'school_status_male2_yes' => $this->filterBySchoolStatus($query1, 'male', 6, 18, 1),
                'school_status_female2_yes' => $this->filterBySchoolStatus($query1, 'female', 6, 18, 1),
                'school_status_male2_no' => $this->filterBySchoolStatus($query1, 'male', 6, 18, 0),
                'school_status_female2_no' => $this->filterBySchoolStatus($query1, 'female', 6, 18, 0),
                'school_status_male3_yes' => $this->filterBySchoolStatus($query1, 'male', 18, null, 1),
                'school_status_female3_yes' => $this->filterBySchoolStatus($query1, 'female', 18, null, 1),
                'school_status_male3_no' => $this->filterBySchoolStatus($query1, 'male', 18, null, 0),
                'school_status_female3_no' => $this->filterBySchoolStatus($query1, 'female', 18, null, 0),
                'school_status_yes' => $query1->where('school_status', 1)->count(),
                'school_status_no' => $query1->where('school_status', 0)->count()
            ]);
        }
        return view('literacy_announcement.Reports.reports', compact('wards', 'filter', 'filteredData', 'total_population', 'selectedVars'));
    }

    private function filterByGender($query, string $string, string $string1)
    {
        return $query->where('gender', $string)->where('literacy', $string1)->count();
    }

    private function filterByAgeGroup($query, $gender, $age1, $age2, $literacy)
    {
        if ($age2 == null) {
            return $query->where('age', '>', $age1)->where('gender', $gender)
                ->where('literacy', $literacy)->count();
        }
        return $query->whereBetween('age', [$age1, $age2])->where('gender', $gender)
            ->where('literacy', $literacy)->count();
    }

    private function filterBySchoolStatus($query, $gender, $age1, $age2, $school_satatus)
    {
        if ($age2 == null) {
            return $query->where('age', '>', $age1)->where('gender', $gender)
                ->where('school_status', $school_satatus)->count();
        }
        return $query->whereBetween('age', [$age1, $age2])->where('gender', $gender)
            ->where('school_status', $school_satatus)->count();
    }
}
