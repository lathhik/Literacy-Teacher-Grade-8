<?php

namespace App\Http\Controllers\LiteracyAnnouncement;

use App\Http\Controllers\Controller;
use App\Models\Ward;
use Illuminate\Http\Request;

class WardController extends Controller
{
    public function ward()
    {
        $wards = Ward::all();

        return view('literacy_announcement.add-ward')->with('wards', $wards);
    }

    public function addWard(Request $request)
    {
        $this->validate($request, [
            'ward' => 'required|unique:wards,ward_no'
        ]);

        $ward_no = new Ward();
        $ward_no->ward_no = $request->ward;

        if ($ward_no->save()) {
            return redirect()->back()->with('success', 'Ward No. was successfully added');
        }

    }

    public function editWard($id)
    {
        $ward = Ward::find($id);
        return view('literacy_announcement.edit-ward', compact('ward'));
    }

    public function editWardAction(Request $request, $id)
    {
        $this->validate($request, [
            'ward' => 'required|unique:wards,ward_no'
        ]);

        $ward = Ward::find($id);

        $ward->ward_no = $request->ward;

        $ward->save();
        return redirect()->route('ward');
    }

}
