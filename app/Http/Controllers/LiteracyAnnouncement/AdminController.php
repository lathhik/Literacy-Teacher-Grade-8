<?php

namespace App\Http\Controllers\LiteracyAnnouncement;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class AdminController extends Controller
{
    public function addAdmin()
    {
        $admins = User::where('is_admin', 0)->get();
        return view('literacy_announcement.admin.add-admin', compact('admins'));
    }

    public function addAdminAction(Request $request)
    {
        $this->validate($request, [
            'full_name' => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required|confirmed',
            'privilege' => 'required'
        ]);

        $admin = new User();
        $admin->name = $request->full_name;
        $admin->email = $request->email;
        $admin->privilege = $request->privilege;
        $admin->password = bcrypt($request->password);

        $admin->save();

        return redirect()->back();

    }

    public function editAdmin($id)
    {
        $admin = User::find($id);
        if ($admin->is_admin == 0) {
            return view('admin.edit-admin', compact('admin'));
        }
        return redirect()->back();
    }


    public function editAdminAction(Request $request, $id)
    {
        $admin = User::find($id);


        $this->validate($request, [
            'full_name' => 'required',
            'privilege' => 'required',
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore($admin->id)
            ],
            'password' => [
                'nullable',
                'min:6',
            ]
        ]);

        $admin->name = $request->full_name;
        $admin->email = $request->email;
        $admin->privilege = $request->privilege;

        if ($request->password) {
            $admin->password = bcrypt($request->password);
        }
        $admin->save();
        return redirect()->route('add-admin');

    }

    public function deleteAdmin($id)
    {
        $admin = User::find($id);

        $admin->delete();

        return redirect()->back();
    }
}
