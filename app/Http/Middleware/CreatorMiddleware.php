<?php

namespace App\Http\Middleware;

use Closure;
use http\Env\Response;

class CreatorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() && $request->user()->privilege == '') {
            return redirect()->back();
        }

        return $next($request);

    }
}
