<?php

namespace App\TsmModels;

use Illuminate\Database\Eloquent\Model;

class TsmFile extends Model
{
    protected $table = 'tsm_files';

    public function tsmFile()
    {
        return $this->hasOne(Teacher::class, 'teacher_id');
    }
}
