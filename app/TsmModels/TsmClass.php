<?php

namespace App\TsmModels;

use Illuminate\Database\Eloquent\Model;

class TsmClass extends Model
{
    protected $table = 'tsm_classes';
}
