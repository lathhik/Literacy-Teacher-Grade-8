<?php

namespace App\TsmModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\User as Authenticatable;

class TsmAdmin extends Authenticatable
{
    protected $table = 'tsm_admins';
    protected $guard = 'tsmAdmin';
}
