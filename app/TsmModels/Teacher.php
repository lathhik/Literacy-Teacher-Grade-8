<?php

namespace App\TsmModels;

use App\TsmModel\Subject;
use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $table = 'teachers';

    public function class()
    {
        return $this->belongsTo(TsmClass::class, 'class_id');
    }

    public function grade()
    {
        return $this->belongsTo(TsmGrade::class);
    }

    public function file()
    {
        return $this->hasMany(TsmFile::class);
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class, 'subject_id');
    }

    public function school()
    {
        return $this->belongsTo(School::class, 'school_id');
    }
}
