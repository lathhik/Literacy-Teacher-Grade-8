<?php

namespace App\TsmModels;

use Illuminate\Database\Eloquent\Model;

class TsmGrade extends Model
{
    protected $table = 'tsm_grades';
}
