<?php

namespace App\TsmModels;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $table = 'tsm_subjects';
}
