<?php

namespace App\TsmModels;

use Illuminate\Database\Eloquent\Model;

class AdminStaff extends Model
{
    protected $table = 'admin_staffs';
}
