<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class House extends Model
{
    public function members()
    {
        return $this->hasMany(MemberDetails::class, 'house_id');
    }

    public function tole()
    {
        return $this->belongsTo(Tole::class, 'tole_id');
    }
}
