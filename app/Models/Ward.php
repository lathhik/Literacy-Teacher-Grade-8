<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ward extends Model
{
    public function toles()
    {
        return $this->hasMany(Tole::class,'ward_id');
    }
}
