<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tole extends Model
{
    public function ward()
    {
        return $this->belongsTo(Ward::class, 'ward_id');
    }

    public function houses(){
        return $this->hasMany(House::class);
    }
}
