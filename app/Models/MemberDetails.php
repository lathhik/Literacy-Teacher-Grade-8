<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberDetails extends Model
{
    public function getFullNameAttribute()
    {
        return $this->first_name. ' ' .$this->middle_name. ' ' .$this->last_name;
    }

    public function house()
    {
        return $this->belongsTo(House::class, 'house_id');
    }
}
