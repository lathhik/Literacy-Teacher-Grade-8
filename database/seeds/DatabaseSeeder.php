<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
//        \App\User::create([
//            'name' => 'Diprung Technologies',
//            'email' => 'diprungtech@gmail.com',
//            'password' => Hash::make('Nepal123'),
//            'privilege' => 'super_admin',
//            'is_admin' => 1
//        ]);

        \App\TsmModels\TsmAdmin::create([
            'full_name' => 'John Snow',
            'email' => 'john@gmail.com',
            'password' => Hash::make('john002'),
            'privilege' => 1
        ]);
    }
}
