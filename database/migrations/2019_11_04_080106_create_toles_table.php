<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('toles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tole');
            $table->integer('ward_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('toles',function (Blueprint $table){
           $table->foreign('ward_id')->references('id')->on('wards');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('toles');
    }
}
