<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminStaffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('admin_staffs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('staff_position_id')->unsigned();
            $table->integer('school_id')->unsigned()->nullable();
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name');
            $table->string('permanent_address');
            $table->string('temporary_address');
            $table->date('date_of_birth');
            $table->string('email');
            $table->bigInteger('phone');
            $table->string('designation_date');
            $table->string('designation_type');
            $table->string('image');
            $table->timestamps();
        });

        Schema::table('admin_staffs', function (Blueprint $table) {
            $table->foreign('staff_position_id')->references('id')->on('staff_positions');
            $table->foreign('school_id')->references('id')->on('schools');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_staffs');
    }
}
