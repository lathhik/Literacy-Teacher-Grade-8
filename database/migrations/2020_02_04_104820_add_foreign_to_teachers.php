<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToTeachers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('teachers', function (Blueprint $table) {
            $table->integer('school_id')->unsigned()->after('grade_id');
            $table->integer('subject_id')->unsigned()->after('school_id');
            $table->date('designation_date')->after('dob');
        });

        Schema::table('teachers', function (Blueprint $table) {
            $table->foreign('school_id')->references('id')->on('schools');
            $table->foreign('subject_id')->references('id')->on('tsm_subjects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('teachers', function (Blueprint $table) {
            $table->dropForeign(['school_id']);
            $table->dropForeign(['subject_id']);
            $table->dropColumn('subject_id');
            $table->dropColumn('school_id');
            $table->dropColumn('designation_date');
        });
    }
}
