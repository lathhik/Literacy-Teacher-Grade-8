<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTsmClassTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tsm_classes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('level');
            $table->enum('class', ['FC', 'SC', 'TC'])->comment('FA is First Class, SA is Second Class and TC is Third Class');
            $table->bigInteger('salary_scale');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tsm_groups');
    }
}
